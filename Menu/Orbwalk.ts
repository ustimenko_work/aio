import { Menu } from "wrapper/Imports";

export enum OrbwalkMode {
	MOVE_TO_MOUSE,
	MOVE_TO_TARGET
}

export interface OrbwalkMenu {
	DangerRange: Menu.Slider;
	OrbwalkingMode: Menu.Dropdown;
	BaseNodeOrbwalk: Menu.Node;
	DangerMoveToMouse: Menu.Toggle;
	OrbwalkerStopOnStanding: Menu.Toggle;
}

export interface IOrbwalkMenuDefault {
	defaultMode?: OrbwalkMode,
	defaultStopOnStanding?: boolean
	defaultDangerRangeState?: boolean
	defaultDangerRangeValue?: number
}

export const OrbwalkMenu = (menu: Menu.Node, option: IOrbwalkMenuDefault): OrbwalkMenu => {

	const BaseNodeOrbwalk = menu.AddNode("COrbwalk", undefined, "Orbwalk or just attack")
	const OrbwalkingMode = BaseNodeOrbwalk.AddDropdown("Orbwalk mode", ["Move to mouse", "Move to target"], option.defaultMode ?? 0)
	const OrbwalkerStopOnStanding = BaseNodeOrbwalk.AddToggle("Stop orbwalk if standing", option.defaultStopOnStanding ?? true, "Unit will not orbwalk if target is standing")
	const DangerMoveToMouse = BaseNodeOrbwalk.AddToggle("Danger move to mouse", option.defaultDangerRangeState ?? false, "Unit will move to mouse without attacking when in danger range")
	const DangerRange = BaseNodeOrbwalk.AddSlider("CDangerRange", (option.defaultDangerRangeValue ?? 0), 0, 1200, 0, "Unit will not move closer to the target")

	return {
		DangerRange,
		OrbwalkingMode,
		BaseNodeOrbwalk,
		DangerMoveToMouse,
		OrbwalkerStopOnStanding,
	}
}

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["COrbwalk", "Орбвалк"],
	["CDangerRange", "Радиус опасности"],
	["Orbwalk mode", "Режим орбвалка"],
	["Move to target", "Двигатся к цели"],
	["Move to mouse", "Двигаться к мышке"],
	["Stop orbwalk if standing", "Останавливать орбвалк"],
	["Danger move to mouse", "В опасности двигаться к мышке"],
	["Orbwalk or just attack", "Использовать орбвалкер или просто атаковать"],
	["Unit will not move closer to the target", "Юнит не будет двигаться ближе к цели"],
	["Unit will not orbwalk if target is standing", "Не использовать орбвалк, если цель не двигается"],
	["Unit will move to mouse without attacking when in danger range", "В радиусе опасности юнит будет двигаться к мышке не атакуя врага"],
]))

Menu.Localization.AddLocalizationUnit("english", new Map([
	["COrbwalk", "Orbwalk"],
	["CDangerRange", "Danger range"],
]))