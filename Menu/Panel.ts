import { Menu } from "wrapper/Imports"

export interface SwitchPanel {
	PanelState: Menu.Toggle;
	PanelSize: Menu.Slider;
	PanelSelector: Menu.ImageSelector;
	PanelOpacityState: Menu.Slider;
	PanelPositionX: Menu.Slider;
	PanelPositionY: Menu.Slider;
}

export const CreateSwitchPanel = (
	baseNode: Menu.Node,
	abilities: string[],
	enableDefaultItems: string[] = ["item_black_king_bar", "item_blink"],
): SwitchPanel => {
	const PanelTree = baseNode.AddNode("Switch panel")
	const PanelState = PanelTree.AddToggle("State", baseNode.InternalName === "Combo", "Enable abilities and items in game")
	const fillerDefault = abilities.filter(x => enableDefaultItems.includes(x))
	const PanelSelector = PanelTree.AddImageSelector("SwitchPanelAbilities", abilities, new Map(fillerDefault.map(name => [name, true])))

	const PanelSize = PanelTree.AddSlider("Size", 25.9, 12, 30, 1)
	const PanelOpacityState = PanelTree.AddSlider("Opacity", 73, 30, 100)
	const PanelPositionX = PanelTree.AddSlider("Position X", 0, -1500, 500)
	const PanelPositionY = PanelTree.AddSlider("Position Y", baseNode.InternalName !== "Combo" ? -31 : 0, -800, 200)

	return {
		PanelState,
		PanelSize,
		PanelSelector,
		PanelPositionX,
		PanelPositionY,
		PanelOpacityState,
	}
}
