import {
	AbilityX,
	BlackKingBar,
	BlinkDagger,
	Bloodthorn,
	Dagon,
	DiffusalBlade,
	ExMachina,
	Flux,
	Gleipnir,
	HeroX,
	MagneticField,
	MantaStyle,
	MedallionOfCourage,
	MinotaurHorn,
	Mjollnir,
	Nullifier,
	OrbWalker,
	OrchidMalevolence,
	RodOfAtos,
	ScytheOfVyse,
	ShivasGuard,
	SolarCrest,
	SparkWraith,
	SpiritVessel,
	TargetManager,
	TempestDouble,
	UnitX,
	UrnOfShadows,
	VeilOfDiscord,
} from "immortal-core/Imports";
import { ArrayExtensions, npc_dota_hero_arc_warden } from "wrapper/Imports";
import {
	AbilityHelper,
	AIOBlink,
	AIOBloodthorn,
	AIOBuff,
	AIODebuff,
	AIODisable,
	AIONuke,
	AIONullifier,
	AIOShield,
	AIOUntargetable,
	BaseHeroCombo,
	BaseOrbwalk,
	HERO_DATA,
	OrbWallker,
	RegisterHeroModule,
	ShieldBreaker,
	ShieldBreakerMap,
	SwitchPanel,
} from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";
import { ArcWardenMagneticField } from "./Abilities/MagneticField";
import { ArcWardenSpark } from "./Abilities/Spark";
import { ArcMenu } from "./menu";

@RegisterHeroModule(npc_dota_hero_arc_warden, ArcMenu)
export class ArcWardenCombo extends BaseHeroCombo {

	public MainHero: Nullable<HeroX>
	public TempestHero: Nullable<HeroX>

	public readonly Ability = {
		vessel: [] as AIODebuff[],
		urn: [] as AIODebuff[],
		shiva: [] as AIODebuff[],
		dagon: [] as AIONuke[],
		manta: [] as AIOBuff[],
		bkb: [] as AIOShield[],
		hex: [] as AIODisable[],
		flux: [] as AIODebuff[],
		blink: [] as AIOBlink[],
		veil: [] as AIODebuff[],
		horn: [] as AIOShield[],
		orchid: [] as AIODisable[],
		atos: [] as AIODisable[],
		gleipnir: [] as AIODisable[],
		spark: [] as ArcWardenSpark[],
		bloodhorn: [] as AIOBloodthorn[],
		exMachina: [] as AIOUntargetable[],
		nullifier: [] as AIONullifier[],
		medal: [] as AIODebuff[],
		solar: [] as AIODebuff[],
		mjollnir: [] as AIOShield[],
		diffusal: [] as AIODebuff[],
		field: [] as ArcWardenMagneticField[],
		tempest: [] as AIOUntargetable[],
	}

	public Combo(unit: UnitX, menu: ModeCombo) {

		const target = TargetManager.Target!
		const shield = ShieldBreakerMap.get(unit)
		this.AbilityHelper = new AbilityHelper(unit, menu, false, true)

		if (shield !== undefined)
			shield.Update(menu)

		if (this.TempestHero !== undefined && !this.TempestHero.HasBuffByName("modifier_kill"))
			this.TempestHero = undefined

		if (!TargetManager.HasValidTarget || !unit.IsAlive || HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || unit.Distance(target) > 1300)
			return false

		if (!this.TempestHero?.HasBuffByName("modifier_kill"))
			if (this.Ability.tempest.some(x => this.AbilityHelper.UseAbility(x)))
				return true

		if (this.Ability.blink.some(x => this.AbilityHelper.UseAbility(x, 600, 350)))
			return true

		if (this.Ability.hex.some(x => this.AbilityHelper.UseAbility(x))) {
			HERO_DATA.ComboSleeper.ExtendSleep(0.3, unit.Handle)
			OrbWalker.Sleeper.Sleep(0.1, unit.Handle)
			return true
		}

		if (this.Ability.veil.some(x => this.AbilityHelper.UseAbility(x))) {
			HERO_DATA.ComboSleeper.ExtendSleep(0.1, unit.Handle)
			OrbWalker.Sleeper.Sleep(0.1, unit.Handle)
			return true
		}

		if (this.Ability.dagon.some(x => this.AbilityHelper.UseAbility(x)))
			return true

		if (this.Ability.bkb.some(x => this.AbilityHelper.UseAbility(x, 800)))
			return true

		if (this.Ability.horn.some(x => this.AbilityHelper.UseAbility(x, 800)))
			return true

		if (this.Ability.flux.some(x => this.AbilityHelper.UseAbility(x))) {
			HERO_DATA.ComboSleeper.ExtendSleep(0.2, unit.Handle)
			OrbWalker.Sleeper.Sleep(0.1, unit.Handle)
			return true
		}

		if (this.Ability.gleipnir.some(x => this.AbilityHelper.UseAbility(x))) {
			HERO_DATA.ComboSleeper.ExtendSleep(0.2, unit.Handle)
			OrbWalker.Sleeper.Sleep(0.1, unit.Handle)
			return true
		}

		if (this.Ability.atos.some(x => this.AbilityHelper.UseAbility(x))) {
			HERO_DATA.ComboSleeper.ExtendSleep(0.2, unit.Handle)
			OrbWalker.Sleeper.Sleep(0.1, unit.Handle)
			return true
		}

		if (this.Ability.bloodhorn.some(x => this.AbilityHelper.UseAbility(x))) {
			HERO_DATA.ComboSleeper.ExtendSleep(0.2, unit.Handle)
			OrbWalker.Sleeper.Sleep(0.1, unit.Handle)
			return true
		}

		if (this.Ability.orchid.some(x => this.AbilityHelper.UseAbility(x))) {
			HERO_DATA.ComboSleeper.ExtendSleep(0.2, unit.Handle)
			OrbWalker.Sleeper.Sleep(0.1, unit.Handle)
			return true
		}

		if (this.Ability.nullifier.some(x => this.AbilityHelper.UseAbility(x))) {
			HERO_DATA.ComboSleeper.ExtendSleep(0.2, unit.Handle)
			OrbWalker.Sleeper.Sleep(0.1, unit.Handle)
			return true
		}

		if (this.Ability.manta.some(x => this.AbilityHelper.UseAbility(x, unit.GetAttackRange(target)))) {
			HERO_DATA.ComboSleeper.ExtendSleep(0.1, unit.Handle)
			OrbWalker.Sleeper.Sleep(0.1, unit.Handle)
			return true
		}

		if (this.Ability.diffusal.some(x => this.AbilityHelper.UseAbility(x))) {
			HERO_DATA.ComboSleeper.ExtendSleep(0.1, unit.Handle)
			OrbWalker.Sleeper.Sleep(0.1, unit.Handle)
			return true
		}

		if (this.Ability.spark.some(x => this.AbilityHelper.UseAbility(x))) {
			HERO_DATA.ComboSleeper.ExtendSleep(0.1, unit.Handle)
			OrbWalker.Sleeper.Sleep(0.1, unit.Handle)
			return true
		}

		if (this.Ability.field.some(x => this.AbilityHelper.UseAbility(x)))
			return true

		if (this.Ability.mjollnir.some(x => this.AbilityHelper.UseAbility(x, 500))) {
			HERO_DATA.ComboSleeper.ExtendSleep(0.1, unit.Handle)
			OrbWalker.Sleeper.Sleep(0.1, unit.Handle)
			return true
		}

		if (this.Ability.shiva.some(x => this.AbilityHelper.UseAbility(x)))
			return true

		if (this.Ability.vessel.some(x => this.AbilityHelper.UseAbility(x)))
			return true

		if (this.Ability.urn.some(x => this.AbilityHelper.UseAbility(x)))
			return true

		if (this.Ability.urn.some(x => this.AbilityHelper.UseAbility(x)))
			return true

		if (this.Ability.solar.some(x => this.AbilityHelper.UseAbility(x)))
			return true

		if (this.Ability.medal.some(x => this.AbilityHelper.UseAbility(x)))
			return true

		if (this.Ability.exMachina.some(x => this.AbilityHelper.UseAbilityIfConditionAnyNotNone(x,
			this.Ability.hex.filter(z => x.Owner.Equals(z.Owner))[0],
			this.Ability.bkb.filter(z => x.Owner.Equals(z.Owner))[0],
		))) return true

		return false
	}

	public onAbilityCreated(owner: UnitX, abil: AbilityX): void {
		if (!this.IsValidHeroName(owner))
			return
		if (abil instanceof Dagon)
			this.Ability.dagon.push(new AIONuke(abil))
		if (abil instanceof Flux)
			this.Ability.flux.push(new AIODebuff(abil))
		if (abil instanceof MantaStyle)
			this.Ability.manta.push(new AIOBuff(abil))
		if (abil instanceof OrchidMalevolence)
			this.Ability.orchid.push(new AIODisable(abil))
		if (abil instanceof MinotaurHorn)
			this.Ability.horn.push(new AIOShield(abil))
		if (abil instanceof BlackKingBar)
			this.Ability.bkb.push(new AIOShield(abil))
		if (abil instanceof ScytheOfVyse)
			this.Ability.hex.push(new AIODisable(abil))
		if (abil instanceof BlinkDagger)
			this.Ability.blink.push(new AIOBlink(abil))
		if (abil instanceof SparkWraith)
			this.Ability.spark.push(new ArcWardenSpark(abil))
		if (abil instanceof Bloodthorn)
			this.Ability.bloodhorn.push(new AIOBloodthorn(abil))
		if (abil instanceof MagneticField)
			this.Ability.field.push(new ArcWardenMagneticField(abil, this.Menu))
		if (abil instanceof RodOfAtos)
			this.Ability.atos.push(new AIODisable(abil))
		if (abil instanceof Gleipnir)
			this.Ability.gleipnir.push(new AIODisable(abil))
		if (abil instanceof ShivasGuard)
			this.Ability.shiva.push(new AIODebuff(abil))
		if (abil instanceof ExMachina)
			this.Ability.exMachina.push(new AIOUntargetable(abil))
		if (abil instanceof UrnOfShadows)
			this.Ability.urn.push(new AIODebuff(abil))
		if (abil instanceof SpiritVessel)
			this.Ability.vessel.push(new AIODebuff(abil))
		if (abil instanceof Nullifier)
			this.Ability.nullifier.push(new AIONullifier(abil))
		if (abil instanceof MedallionOfCourage)
			this.Ability.medal.push(new AIODebuff(abil))
		if (abil instanceof SolarCrest)
			this.Ability.solar.push(new AIODebuff(abil))
		if (abil instanceof Mjollnir)
			this.Ability.mjollnir.push(new AIOShield(abil))
		if (abil instanceof DiffusalBlade)
			this.Ability.diffusal.push(new AIODebuff(abil))
		if (abil instanceof VeilOfDiscord)
			this.Ability.veil.push(new AIODebuff(abil))
		if (abil instanceof TempestDouble)
			this.Ability.tempest.push(new AIOUntargetable(abil))
	}

	public onAbilityDestroyed(owner: UnitX, abil: AbilityX): void {
		const abilities = this.Ability
		this.RemoveAbilities(abilities.hex, owner, abil)
		this.RemoveAbilities(abilities.flux, owner, abil)
		this.RemoveAbilities(abilities.blink, owner, abil)
		this.RemoveAbilities(abilities.field, owner, abil)
		this.RemoveAbilities(abilities.spark, owner, abil)
		this.RemoveAbilities(abilities.blink, owner, abil)
		this.RemoveAbilities(abilities.bkb, owner, abil)
		this.RemoveAbilities(abilities.horn, owner, abil)
		this.RemoveAbilities(abilities.orchid, owner, abil)
		this.RemoveAbilities(abilities.bloodhorn, owner, abil)
		this.RemoveAbilities(abilities.atos, owner, abil)
		this.RemoveAbilities(abilities.gleipnir, owner, abil)
		this.RemoveAbilities(abilities.exMachina, owner, abil)
		this.RemoveAbilities(abilities.manta, owner, abil)
		this.RemoveAbilities(abilities.dagon, owner, abil)
		this.RemoveAbilities(abilities.shiva, owner, abil)
		this.RemoveAbilities(abilities.urn, owner, abil)
		this.RemoveAbilities(abilities.vessel, owner, abil)
		this.RemoveAbilities(abilities.nullifier, owner, abil)
		this.RemoveAbilities(abilities.medal, owner, abil)
		this.RemoveAbilities(abilities.solar, owner, abil)
		this.RemoveAbilities(abilities.mjollnir, owner, abil)
		this.RemoveAbilities(abilities.diffusal, owner, abil)
		this.RemoveAbilities(abilities.veil, owner, abil)
		this.RemoveAbilities(abilities.tempest, owner, abil)
	}

	public onCreated(owner: UnitX) {
		if (!this.IsValidHeroName(owner))
			return

		if (!ShieldBreakerMap.has(owner))
			ShieldBreakerMap.set(owner, new ShieldBreaker(owner, this.Menu.Shields))

		if (!OrbWallker.has(owner.Handle))
			OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, this.Menu))

		const IsTempestDouble = owner.BaseOwner.IsTempestDouble

		if (!owner.IsIllusion && !IsTempestDouble)
			this.MainHero = owner

		if (IsTempestDouble)
			this.TempestHero = owner
	}

	public onDestoyed(owner: UnitX) {
		super.onDestoyed(owner)

		if (!this.IsValidHeroName(owner))
			return

		const IsTempestDouble = owner.BaseOwner.IsTempestDouble

		if (!owner.IsIllusion && !IsTempestDouble)
			this.MainHero = undefined

		if (IsTempestDouble)
			this.TempestHero = undefined
	}

	public onGameEnded() {
		super.onGameEnded()
		this.Ability.shiva = []
		this.Ability.dagon = []
		this.Ability.manta = []
		this.Ability.bkb = []
		this.Ability.hex = []
		this.Ability.flux = []
		this.Ability.blink = []
		this.Ability.field = []
		this.Ability.horn = []
		this.Ability.orchid = []
		this.Ability.atos = []
		this.Ability.gleipnir = []
		this.Ability.spark = []
		this.Ability.bloodhorn = []
		this.Ability.exMachina = []
		this.Ability.vessel = []
		this.Ability.urn = []
		this.Ability.solar = []
		this.Ability.medal = []
		this.Ability.mjollnir = []
		this.Ability.diffusal = []
		this.Ability.veil = []
		this.Ability.tempest = []
		this.MainHero = undefined
		this.TempestHero = undefined
	}

	public onTick(unit: UnitX) {
		if (!this.Menu.BaseState.value)
			return

		const lockTarget = this.Menu.TargetMenuTree.LockTarget.value
		const deathSwitch = this.Menu.TargetMenuTree.DeathSwitch.value
		const focusTarget = this.Menu.TargetMenuTree.FocusTarget.selected_id
		TargetManager.Update(focusTarget, lockTarget, deathSwitch)
		TargetManager.TargetLocked = this.Menu.ComboKey.is_pressed || this.Menu.HarassKey.is_pressed

		if (unit.CanUseAbilities && unit.CanUseItems) {
			if (this.Menu.ComboKey.is_pressed && this.MainHero !== undefined)
				TargetManager.Owner = unit
			else if (this.Menu.HarassKey.is_pressed && this.TempestHero !== undefined)
				TargetManager.Owner = this.TempestHero
			else
				TargetManager.Owner = unit
		}

		this.ComboMode(
			unit,
			this.Menu.ComboMode,
			this.Menu.ComboKey,
		)

		if (unit.BaseOwner.IsTempestDouble || unit.IsIllusion) {
			this.ComboMode(
				unit,
				this.Menu.HarrasMode,
				this.Menu.HarassKey,
				unit,
			)
		}
	}

	public onDraw(owner: UnitX) {
		if (!this.Menu.BaseState.value || (owner.IsIllusion && !owner.BaseOwner.IsTempestDouble))
			return

		TargetManager.Draw(
			owner,
			this.Menu.TargetMenuTree.DrawTargetParticle.value,
			this.Menu.TargetMenuTree.DrawTargetParticleColor.selected_color,
			this.Menu.TargetMenuTree.DrawTargetParticleColor2.selected_color,
		)

		SwitchPanel.Draw(this.Menu.ComboMode)
		SwitchPanel.Draw(this.Menu.HarrasMode)
	}

	private RemoveAbilities(obj: any, owner: UnitX, abil: AbilityX) {
		for (const ability of obj)
			if (ability.Ability.Handle === abil.Handle && owner.Equals(abil.Owner))
				ArrayExtensions.arrayRemove(obj, ability)
	}
}
