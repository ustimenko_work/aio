import { BaseMenu } from "gitlab.com/SicdeX/immortal-aio/Menu/Base";
import { ActiveAbility, EventsX, OrbWalker, TickSleeperX } from "immortal-core/Imports";
import { npc_dota_hero_arc_warden } from "wrapper/Imports";
import { AIOShield, HERO_DATA } from "_ICore_/Index";
import { ArcWardenCombo } from "../Listeners";

const Sleeper = new TickSleeperX()

export class ArcWardenMagneticField extends AIOShield {

	constructor(public Ability: ActiveAbility, private menu: BaseMenu) {
		super(Ability)
	}

	public ShouldCast() {
		if (Sleeper.Sleeping)
			return false

		if (this.Target === undefined || !this.Owner.IsAlive || this.Owner.IsInvulnerable || this.Owner.Distance(this.Target) > this.Owner.AttackRange(this.Target))
			return false
		if (this.Owner.HasBuffByName(this.Shield.ShieldModifierName))
			return false

		return true
	}

	public UseAbility(aoe: boolean) {

		if (this.Owner.IsInAbilityPhase)
			return true

		const IsCloneCombo = this.menu.HarassKey.is_pressed
		const combo = HERO_DATA.HeroModules.get(npc_dota_hero_arc_warden) as ArcWardenCombo

		let hero = IsCloneCombo ? combo.TempestHero : combo.MainHero
		if (hero === undefined)
			hero = (IsCloneCombo ? combo.MainHero : combo.TempestHero) ?? this.Owner

		if (this.Target.GetAngle(this.Owner) > 1) {
			if (!this.Owner.UseAbility(this.Ability, {intent: hero.InFront(this.Ability.Radius / 2)}))
				return false
			Sleeper.Sleep(1)
			const delay = this.Ability.GetCastDelay(this.Owner)
			HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
			this.Ability.ActionSleeper.Sleep(delay + 0.5)
			OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
			return true
		}

		const Radius = IsCloneCombo
			? -this.Ability.Radius / 2
			: this.Owner.BaseOwner.IsTempestDouble
				? -(this.Ability.Radius - this.Owner.HullRadius / 2)
				: (this.Ability.Radius - this.Owner.HullRadius / 2)

		const Position = hero.InFront(Radius)

		if (!this.Owner.UseAbility(this.Ability, {intent: Position}))
			return false

		const delay = this.Ability.GetCastDelay(this.Owner)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}

EventsX.on("GameEnded", () => Sleeper.ResetTimer())
