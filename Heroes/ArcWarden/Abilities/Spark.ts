import { OrbWalker } from "immortal-core/Imports";
import { AIONuke, HERO_DATA } from "_ICore_/Index";

export class ArcWardenSpark extends AIONuke {

	public UseAbility(aoe: boolean) {

		if (this.Target === undefined || !this.Owner.IsAlive)
			return false

		if (this.Owner.IsInAbilityPhase)
			return true

		if (this.Target.IdealSpeed < 400)
			return super.UseAbility(aoe)

		const Position = this.Target.IsMoving
			? this.Target.InFront(this.Target.IdealSpeed + 200)
			: this.Target.Position

		if (!this.Owner.UseAbility(this.Ability, {intent: Position}))
			return false

		const delay = this.Ability.GetCastDelay(this.Target)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
