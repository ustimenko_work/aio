import { Attributes, Menu } from "wrapper/Imports";
import { BaseHeroMenu, BaseItemsNames } from "../../Menu/Base";

const excludeItem = "item_abyssal_blade"

export const ArcMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_arc_warden",
	ComboName: "Full combo",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	AlternativeHarassName: "Tempest Double combo",
	Items: [
		"item_blink",
		"item_manta",
		...BaseItemsNames(excludeItem),
	],
	Abilities: [
		"arc_warden_flux",
		"arc_warden_magnetic_field",
		"arc_warden_spark_wraith",
		"arc_warden_tempest_double",
	],
	HarassAbilities: [
		"arc_warden_flux",
		"arc_warden_magnetic_field",
		"arc_warden_spark_wraith",
	],
	Shields: {
		IncludeLinkenBreak: ["arc_warden_flux"],
		IncludeSpellsShield: ["arc_warden_flux"],
		ExcludeItemsLinkenBreak: excludeItem,
		ExcludeItemsSpellsShield: excludeItem,
	},
	OrbwalkDefault: {
		defaultDangerRangeValue: 350,
	},
})

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["Full combo", "Полное комбо"],
	["Tempest Double combo", "Tempest Double комбо"],
]))
