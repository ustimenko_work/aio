import { KillSteal } from "immortal-kill-steal/Imports";
import {
	AbilityX,
	AbyssalBlade,
	SoulRing,
	ActiveAbility,
	ArmletOfMordiggian,
	BlackKingBar,
	BladeMail,
	BlinkDagger,
	Bloodthorn,
	Dagon,
	DiffusalBlade,
	EtherealBlade,
	EulsScepterOfDivinity,
	EventsX,
	ExMachina,
	ForceStaff,
	GhostScepter,
	Gleipnir,
	GlimmerCape,
	GuardianGreaves,
	LotusOrb,
	MantaStyle,
	MaskOfMadness,
	MedallionOfCourage,
	MinotaurHorn,
	Mjollnir,
	Nullifier,
	OnExecuteOrderX,
	OrbWalker,
	OrchidMalevolence,
	RefresherOrb,
	RefresherShard,
	RodOfAtos,
	ScytheOfVyse,
	ShivasGuard,
	SolarCrest,
	SpiderLegs,
	SpiritVessel,
	TargetManager,
	UnitX,
	UnitXManager,
	UrnOfShadows,
	VeilOfDiscord,
	WindWaker,
} from "immortal-core/Imports";
import { dotaunitorder_t, Entity, ExecuteOrder, Input, Menu, Unit, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseMenu, ModeCombo } from "../../Menu/Base";
import { AIOBuff } from "../Abilities/Buff";
import { AIODebuff } from "../Abilities/Debuff";
import { AIODisable } from "../Abilities/Disable";
import {
	AIOBlackKingBar,
	AIOBlink,
	AIOBlinkDagger,
	AIOBloodthorn,
	AIOEtherealBlade,
	AIOEulsScepter,
	AIOForceStaff,
	AIOGhostScepter,
	AIOMinotaurHorn,
	AIONuke,
	AIONullifier,
	AIOOrchid,
	AIOSpeedBuff,
	AIOUntargetable,
} from "../Abilities/Index";
import { AIOShield } from "../Abilities/Shield";
import { HeroModule, HERO_DATA, ModeAttack } from "../data";
import { AbilityHelper } from "../Manager/Abilities";
import { OrbWallker, OrbWallkerManager } from "../Manager/Orbwalk";
import { ShieldBreaker, ShieldBreakerMap } from "../Manager/ShieldBreaker";
import { BaseOrbwalk } from "./Orbwalk";
import { SwitchPanel } from "./Panels";

export function RegisterHeroModule(unit_id: Constructor<Unit> | string, Menu: BaseMenu) {
	return (constructor: object) => {
		HERO_DATA.HeroModules.set(unit_id, new (constructor as Constructor<HeroModule>)(Menu))
	}
}

export abstract class BaseHeroCombo {

	public AbilityHelper!: AbilityHelper
	public OldStateModeAttack = HERO_DATA.ModeAttack
	public Items = {
		greaves: undefined as Nullable<AIOUntargetable>,
		glimer: undefined as Nullable<AIOShield>,
		ghost: undefined as Nullable<AIOGhostScepter>,
		dagon: undefined as Nullable<AIONuke>,
		manta: undefined as Nullable<AIOBuff>,
		bkb: undefined as Nullable<AIOShield>,
		hex: undefined as Nullable<AIODisable>,
		medal: undefined as Nullable<AIODebuff>,
		solar: undefined as Nullable<AIODebuff>,
		orchid: undefined as Nullable<AIOOrchid>,
		diffusal: undefined as Nullable<AIODebuff>,
		abyssal: undefined as Nullable<AIODisable>,
		minotavr: undefined as Nullable<AIOShield>,
		nullifier: undefined as Nullable<AIONullifier>,
		forceStaff: undefined as Nullable<AIOForceStaff>,
		bladeMail: undefined as Nullable<AIOShield>,
		shiva: undefined as Nullable<AIODebuff>,
		mjollnir: undefined as Nullable<AIOShield>,
		armlet: undefined as Nullable<AIOBuff>,
		atos: undefined as Nullable<AIODisable>,
		blink: undefined as Nullable<AIOBlink>,
		urn: undefined as Nullable<AIODebuff>,
		vessel: undefined as Nullable<AIODebuff>,
		gleipnir: undefined as Nullable<AIODisable>,
		veil: undefined as Nullable<AIODebuff>,
		eul: undefined as Nullable<AIOEulsScepter>,
		waker: undefined as Nullable<AIOEulsScepter>,
		refresher: undefined as Nullable<AIOUntargetable>,
		refresherShard: undefined as Nullable<AIOUntargetable>,
		bloodthorn: undefined as Nullable<AIOBloodthorn>,
		exMachina: undefined as Nullable<AIOUntargetable>,
		blinkCondition: undefined as Nullable<AIOBlinkDagger>,
		maskOfMadness: undefined as Nullable<AIOBuff>,
		lotus: undefined as Nullable<AIOShield>,
		soulRing: undefined as Nullable<AIOUntargetable>,
		spiderLegs: undefined as Nullable<AIOSpeedBuff>,
		etherealBlade: undefined as Nullable<AIOEtherealBlade>,
	}
	private ActiveCombos = 0

	constructor(public readonly Menu: BaseMenu) {
		this.RemoveControllable()
		this.OnReleasePressedMode()
	}

	private get IsValidPressedUnit() {
		if (!this.Menu.BaseState.value)
			return false
		return TargetManager.Heroes.some(x => x.IsControllable && !x.IsEnemy() && this.IsValidHeroName(x))
	}

	public abstract Combo(unit: UnitX, menu: ModeCombo): boolean

	public IsValidHeroName(owner: UnitX) {
		return this.Menu.HeroName === owner.Name
	}

	public onOrders(order: ExecuteOrder) {
		if (!this.Menu.BaseState.value)
			return true

		if (order.OrderType === dotaunitorder_t.DOTA_UNIT_ORDER_HOLD_POSITION || order.OrderType === dotaunitorder_t.DOTA_UNIT_ORDER_CONTINUE)
			this.RefreshUnitState(order)

		return !(TargetManager.HasValidTarget && TargetManager.TargetLocked)
	}

	public onOrdersScript(order: OnExecuteOrderX) {
	}

	public onTick(unit: UnitX) {
		if (!this.Menu.BaseState.value)
			return

		if (unit.CanUseAbilities && unit.CanUseItems)
			TargetManager.Owner = unit

		const lockTarget = this.Menu.TargetMenuTree.LockTarget.value
		const deathSwitch = this.Menu.TargetMenuTree.DeathSwitch.value
		const focusTarget = this.Menu.TargetMenuTree.FocusTarget.selected_id
		TargetManager.Update(focusTarget, lockTarget, deathSwitch)
		TargetManager.TargetLocked = this.Menu.ComboKey.is_pressed || this.Menu.HarassKey.is_pressed

		this.ComboMode(
			unit,
			this.Menu.ComboMode,
			this.Menu.ComboKey,
		)

		this.ComboMode(
			unit,
			this.Menu.HarrasMode,
			this.Menu.HarassKey,
		)
	}

	public onDraw(owner: UnitX) {
		if (!this.Menu.BaseState.value || owner.BaseOwner.IsIllusion)
			return
		TargetManager.Draw(
			owner,
			this.Menu.TargetMenuTree.DrawTargetParticle.value,
			this.Menu.TargetMenuTree.DrawTargetParticleColor.selected_color,
			this.Menu.TargetMenuTree.DrawTargetParticleColor2.selected_color,
		)
		SwitchPanel.Draw(this.Menu.ComboMode)
		SwitchPanel.Draw(this.Menu.HarrasMode)
	}

	public onCreated(owner: UnitX) {
		if (!this.IsValidHeroName(owner))
			return

		if (!ShieldBreakerMap.has(owner))
			ShieldBreakerMap.set(owner, new ShieldBreaker(owner, this.Menu.Shields))

		if (!OrbWallker.has(owner.Handle))
			OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, this.Menu))
	}

	public onDestoyed(owner: UnitX) {
		if (!this.IsValidHeroName(owner))
			return
		OrbWallker.delete(owner.Handle)
		ShieldBreakerMap.delete(owner)
	}

	public onKeyDown(key: VKeys) {
		if (!this.Menu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
			return true
		return !((this.Menu.ComboKey.assigned_key as VKeys) === key
			|| (this.Menu.HarassKey.assigned_key as VKeys) === key)
	}

	public onMouseDown(key: VMouseKeys) {
		if (!this.Menu.BaseState.value)
			return true
		if (key === VMouseKeys.MK_LBUTTON) {
			return !(SwitchPanel.MouseLeftDown(this.Menu.ComboMode) || SwitchPanel.MouseLeftDown(this.Menu.HarrasMode))
		}
		return !(!Input.IsKeyDown(VKeys.CONTROL)
			&& (this.Menu.ComboKey.assigned_key as VMouseKeys) === key
			|| (this.Menu.HarassKey.assigned_key as VMouseKeys) === key)
	}

	public onAbilityCreated(owner: UnitX, abil: AbilityX) {
		if (!this.IsValidHeroName(owner))
			return
		if (abil instanceof SpiderLegs)
			this.Items.spiderLegs = new AIOSpeedBuff(abil)
		if (abil instanceof LotusOrb)
			this.Items.lotus = new AIOShield(abil)
		if (abil instanceof ArmletOfMordiggian)
			this.Items.armlet = new AIOBuff(abil)
		if (abil instanceof GuardianGreaves)
			this.Items.greaves = new AIOUntargetable(abil)
		if (abil instanceof GlimmerCape)
			this.Items.glimer = new AIOShield(abil)
		if (abil instanceof GhostScepter)
			this.Items.ghost = new AIOGhostScepter(abil)
		if (abil instanceof EtherealBlade)
			this.Items.etherealBlade = new AIOEtherealBlade(abil)
		if (abil instanceof MaskOfMadness)
			this.Items.maskOfMadness = new AIOBuff(abil)
		if (abil instanceof ExMachina)
			this.Items.exMachina = new AIOUntargetable(abil)
		if (abil instanceof RodOfAtos)
			this.Items.atos = new AIODisable(abil)
		if (abil instanceof Gleipnir)
			this.Items.gleipnir = new AIODisable(abil)
		if (abil instanceof BladeMail)
			this.Items.bladeMail = new AIOShield(abil)
		if (abil instanceof ForceStaff)
			this.Items.forceStaff = new AIOForceStaff(abil)
		if (abil instanceof BlinkDagger) {
			this.Items.blink = new AIOBlink(abil)
			this.Items.blinkCondition = new AIOBlinkDagger(abil)
		}
		if (abil instanceof Dagon)
			this.Items.dagon = new AIONuke(abil)
		if (abil instanceof MantaStyle)
			this.Items.manta = new AIOBuff(abil)
		if (abil instanceof ScytheOfVyse)
			this.Items.hex = new AIODisable(abil)
		if (abil instanceof BlackKingBar)
			this.Items.bkb = new AIOBlackKingBar(abil)
		if (abil instanceof MinotaurHorn)
			this.Items.minotavr = new AIOMinotaurHorn(abil)
		if (abil instanceof OrchidMalevolence)
			this.Items.orchid = new AIOOrchid(abil)
		if (abil instanceof Bloodthorn)
			this.Items.bloodthorn = new AIOBloodthorn(abil)
		if (abil instanceof Nullifier)
			this.Items.nullifier = new AIONullifier(abil)
		if (abil instanceof AbyssalBlade)
			this.Items.abyssal = new AIODisable(abil)
		if (abil instanceof DiffusalBlade)
			this.Items.diffusal = new AIODebuff(abil)
		if (abil instanceof SolarCrest)
			this.Items.solar = new AIODebuff(abil)
		if (abil instanceof MedallionOfCourage)
			this.Items.medal = new AIODebuff(abil)
		if (abil instanceof ShivasGuard)
			this.Items.shiva = new AIODebuff(abil)
		if (abil instanceof Mjollnir)
			this.Items.mjollnir = new AIOShield(abil)
		if (abil instanceof UrnOfShadows)
			this.Items.urn = new AIODebuff(abil)
		if (abil instanceof SpiritVessel)
			this.Items.vessel = new AIODebuff(abil)
		if (abil instanceof VeilOfDiscord)
			this.Items.veil = new AIODebuff(abil)
		if (abil instanceof WindWaker)
			this.Items.waker = new AIOEulsScepter(abil)
		if (abil instanceof EulsScepterOfDivinity)
			this.Items.eul = new AIOEulsScepter(abil)
		if (abil instanceof RefresherOrb)
			this.Items.refresher = new AIOUntargetable(abil)
		if (abil instanceof RefresherShard)
			this.Items.refresherShard = new AIOUntargetable(abil)
		if (abil instanceof SoulRing)
			this.Items.soulRing = new AIOUntargetable(abil)

	}

	public onAbilityDestroyed(owner: UnitX, abil: AbilityX) {
		const ability = this.Items as any
		for (const key in this.Items) {
			if (ability.hasOwnProperty(key) && ability[key]?.Ability === abil)
				ability[key] = undefined
		}
	}

	public onGameEnded() {
		const ability = this.Items as any
		for (const key in this.Items) {
			if (ability.hasOwnProperty(key))
				ability[key] = undefined
		}
	}

	protected ComboMode(unit: UnitX, mode: ModeCombo, key: Menu.KeyBind, orbWallkerOnce?: UnitX) {
		if (!key.is_pressed)
			return

		if (unit.CanUseAbilities && unit.CanUseItems)
			if (this.Combo(unit, mode))
				return

		orbWallkerOnce === undefined
			? OrbWallkerManager.Update(unit.Handle)
			: OrbWallkerManager.UpdateOneUnit(orbWallkerOnce)
	}

	protected RefreshUnitState(order: ExecuteOrder) {
		if (order.OrderType === dotaunitorder_t.DOTA_UNIT_ORDER_HOLD_POSITION)
			TargetManager.TargetLocked = false
		const owner = order.Issuers[0]
		const ownerX = UnitXManager.GetUnit(owner)
		let targetX: Nullable<UnitX> = undefined
		const target = order.Target
		if (target instanceof Entity)
			targetX = UnitXManager.GetUnit(target)
		if (ownerX === undefined)
			return
		ownerX.Abilities.forEach(abil => {
			if (!(abil instanceof ActiveAbility))
				return
			targetX?.RefreshUnitState()
			abil.ActionSleeper.ResetTimer()
			KillSteal.KillStealSleeper.ResetTimer()
			OrbWalker.Sleeper.ResetKey(ownerX.Handle)
			HERO_DATA.ComboSleeper.ResetKey(ownerX.Handle)
		})
	}

	private RemoveControllable() {
		EventsX.on("removeControllable", owner => {
			if (!this.IsValidHeroName(owner))
				return
			OrbWallker.delete(owner.Handle)
		})
	}

	private OnReleasePressedMode() {

		this.Menu.ComboKey.OnPressed(() => {
			if (!this.IsValidPressedUnit)
				return
			if (this.ActiveCombos++ === 0)
				this.OldStateModeAttack = HERO_DATA.ModeAttack
			if (this.OldStateModeAttack !== ModeAttack.Never)
				HERO_DATA.ModeAttack = ModeAttack.Never
		})

		this.Menu.HarassKey.OnPressed(() => {
			if (!this.IsValidPressedUnit)
				return
			if (this.ActiveCombos++ === 0)
				this.OldStateModeAttack = HERO_DATA.ModeAttack
			if (this.OldStateModeAttack !== ModeAttack.Never)
				HERO_DATA.ModeAttack = ModeAttack.Never
		})

		this.Menu.ComboKey.OnRelease(() => {
			if (this.IsValidPressedUnit && --this.ActiveCombos === 0)
				HERO_DATA.ModeAttack = this.OldStateModeAttack
		})

		this.Menu.HarassKey.OnRelease(() => {
			if (this.IsValidPressedUnit && --this.ActiveCombos === 0)
				HERO_DATA.ModeAttack = this.OldStateModeAttack
		})
	}
}
