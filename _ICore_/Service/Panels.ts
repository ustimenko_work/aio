import { PathX } from "immortal-core/Imports";
import { Color, GUIInfo, Input, LocalPlayer, Menu, Rectangle, RendererSDK, Vector2 } from "wrapper/Imports";
import { ModeCombo } from "../../Menu/Base";

export class SwitchPanel {

	// public readonly MainInventorySlots: Rectangle[] = []
	// public readonly BackpackSlots: Rectangle[] = []

	public static GetItemVectors(
		menu: ModeCombo,
		ItemSize: Vector2,
		gap: Vector2,
	): Map<string, [Menu.ImageSelector, Rectangle]> {
		const map = new Map<string, [Menu.ImageSelector, Rectangle]>()
		if (menu.Panel === undefined || LocalPlayer?.Hero === undefined)
			return map
		const unit = LocalPlayer.Hero
		if (!menu.Panel.PanelState.value || unit === undefined)
			return map

		const HUD = GUIInfo.GetLowerHUDForUnit(unit)
		if (HUD === undefined)
			return map

		const FakeItemSize = ItemSize.Add(gap.MultiplyScalar(2)),
			BasePosition = HUD.InventoryContainer.pos1
				.Clone()
				.SubtractScalarY(FakeItemSize.y)
				.AddScalarX(GUIInfo.ScaleWidth(menu.Panel.PanelPositionX.value))
				.AddScalarY(GUIInfo.ScaleHeight(menu.Panel.PanelPositionY.value)),
			MaxX = BasePosition.x + HUD.InventoryContainer.Width,
			Position = BasePosition.Clone()
		this.Menu(menu).forEach(selector => {
			for (const imageName of selector.values) {
				if (map.has(imageName) || !menu.Panel!.PanelSelector.IsEnabled(imageName))
					continue
				const rect = new Rectangle()
				rect.Width = FakeItemSize.x
				rect.Height = FakeItemSize.y
				rect.x = Position.x
				rect.y = Position.y
				map.set(imageName, [selector, rect])
				Position.x += FakeItemSize.x
				if (Position.x + FakeItemSize.x > MaxX) {
					Position.x = BasePosition.x
					Position.y -= FakeItemSize.y
				}
			}
		})
		return map
	}

	public static Draw(menu: ModeCombo) {
		const ItemSize = this.ItemSize(menu),
			gap = ItemSize.DivideScalar(50).CeilForThis(),
			sizeGap = gap.MultiplyScalar(2),
			opacity = (menu.Panel!.PanelOpacityState.value ?? 0) * 2.55
		for (const [imageName, [selector, rect]] of this.GetItemVectors(menu, ItemSize, gap)) {
			const Image = imageName.includes("item_")
				? PathX.DOTAItems(imageName)
				: PathX.DOTAAbilities(imageName)
			const size = rect.Size
			RendererSDK.OutlinedRect(
				rect.pos1,
				size,
				gap.x,
				selector.IsEnabled(imageName)
					? Color.Green.SetA(opacity)
					: Color.Red.SetA(opacity),
			)
			RendererSDK.Image(
				Image,
				rect.pos1.Add(gap),
				-1,
				rect.Size.SubtractForThis(sizeGap),
				Color.White.SetA(opacity),
			)
		}
	}

	public static MouseLeftDown(menu: ModeCombo): boolean {
		const ItemSize = this.ItemSize(menu),
			gap = ItemSize.DivideScalar(50).CeilForThis(),
			cursor = Input.CursorOnScreen
		for (const [imageName, [selector, rect]] of this.GetItemVectors(menu, ItemSize, gap))
			if (rect.Contains(cursor)) {
				selector.enabled_values.set(imageName, !selector.enabled_values.get(imageName))
				return true
			}
		return false
	}

	private static Menu(menu: ModeCombo) {
		return [menu.Abilities, menu.Items]
	}

	private static ItemSize(menu: ModeCombo) {
		if (menu.Panel === undefined)
			return new Vector2()
		return new Vector2(
			GUIInfo.ScaleWidth(menu.Panel.PanelSize.value),
			GUIInfo.ScaleHeight(menu.Panel.PanelSize.value),
		)
	}
}
