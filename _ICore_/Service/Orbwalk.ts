import { OrbAbility, OrbWalker, Telekinesis, TickSleeperX, UnitX } from "immortal-core/Imports";
import { Flow_t, Input, item_echo_sabre, MathSDK, npc_dota_hero_rubick, Vector3 } from "wrapper/Imports";
import { BaseMenu } from "../../Menu/Base";
import { OrbwalkMode } from "../../Menu/Orbwalk";

export class BaseOrbwalk {

	public LastTarget: Nullable<UnitX>
	public MoveSleeper = new TickSleeperX()
	public AttackSleeper = new TickSleeperX()
	public LastMovePosition = new Vector3()
	protected BodyBlockRange = 150
	protected FreeMoveSleeper = new TickSleeperX()

	constructor(public readonly Owner: UnitX, public readonly Menu: BaseMenu) {
	}

	public get Ping() {
		return GetAvgLatency(Flow_t.IN) * 1000
	}

	public get OrbwalkingMode(): OrbwalkMode {
		return this.OrbwalkMenu.OrbwalkingMode.selected_id
	}

	public set OrbwalkingMode(value) {
		this.OrbwalkMenu.OrbwalkingMode.selected_id = value
	}

	public get Ping2() {
		return this.Ping / 2000
	}

	protected get Mode() {
		return this.Menu.ComboKey.is_pressed ? this.Menu.ComboMode : this.Menu.HarrasMode
	}

	protected get OrbwalkMenu() {
		return this.Menu.OrbWallkerMenuTree
	}

	public OnAttackStart() {
		let attackPoint = this.Owner.GetAttackPoint(this.LastTarget)
		if (this.Owner.Abilities.some(x => x.BaseAbility instanceof item_echo_sabre && x.CanBeCasted()))
			attackPoint *= 2.5
		this.MoveSleeper.Sleep((attackPoint - this.Ping2) + .06)
		this.AttackSleeper.Sleep((attackPoint + this.Owner.GetAttackBackswing(this.LastTarget)) - this.Ping2 - .06)
	}

	public Orbwalk(target: UnitX) {
		let move = this.Mode.Move.value
		if (target !== undefined && this.Owner.IsRanged && this.Owner.HasBuffByName("modifier_item_hurricane_pike_range"))
			move = false
		return this.OrbwalkSet(target, this.Mode.Attack.value, move)
	}

	public CanMove() {
		if (!this.Owner.CanMove())
			return false
		return !this.MoveSleeper.Sleeping
	}

	public Move(movePosition: Vector3) {
		if (!this.CanMove())
			return false
		if (movePosition.Equals(this.LastMovePosition))
			return false
		if (!this.Owner.Move(movePosition))
			return false
		this.LastMovePosition = movePosition
		return true
	}

	public CanAttack(target: Nullable<UnitX>, additionalRange: number = 0) {
		if (target === undefined)
			return false
		if (!this.Owner.CanAttack(target, additionalRange))
			return false
		if (this.OrbwalkMenu.DangerRange.value > 0 && this.OrbwalkMenu.DangerMoveToMouse.value
			&& this.Owner.Distance(target) < Math.min(
				this.Owner.GetAttackRange(),
				this.OrbwalkMenu.DangerRange.value)) {
			return false
		}
		const delay = this.Owner.GetTurnTime(target.Position) + this.Ping2
		if (delay <= 0)
			return !this.AttackSleeper.Sleeping
		return this.AttackSleeper.RemainingSleepTime <= delay
	}

	protected Attack(target: UnitX) {
		if (this.Owner.BaseOwner instanceof npc_dota_hero_rubick) {
			const telekinesis = this.Owner.Abilities.find(x => x instanceof Telekinesis)
			if (telekinesis?.CanBeCasted() === true && this.Mode.Abilities.IsEnabled(telekinesis.Name))
				return false
		}
		if (!this.UseOrbAbility(target) && !this.Owner.Attack(target.Owner))
			return false
		const ping = this.Ping2 + 0.06
		const turnTime = this.Owner.GetTurnTime(target.Position)
		const distance = Math.max(this.Owner.Distance(target) - this.Owner.GetAttackRange(target), 0) / this.Owner.IdealSpeed
		const delay = turnTime + distance + ping
		let attackPoint = this.Owner.GetAttackPoint(target)
		if (this.Owner.Abilities.some(x => x.BaseAbility instanceof item_echo_sabre && x.CanBeCasted()))
			attackPoint *= 2.5
		const attackSleeper = (this.Owner.GetAttackPoint(target) + this.Owner.GetAttackBackswing(target) + delay) - .1
		const moveSleeper = attackPoint + delay + 0.25
		this.AttackSleeper.Sleep(attackSleeper)
		this.MoveSleeper.Sleep(moveSleeper)
		return true
	}

	protected UseOrbAbility(target: UnitX) {
		if (!this.Owner.CanUseAbilities)
			return false

		const ability = this.Owner.Abilities.find(x => x instanceof OrbAbility
			&& this.Mode.Abilities.IsEnabled(x.Name) === true
			&& !x.Enabled
			&& x.CanBeCasted()
			&& x.CanHit(target),
		) as OrbAbility

		if (ability === undefined)
			return false

		if (this.Owner.Mana < ability.ManaCost)
			return this.Owner.Attack(target.Owner)

		return ability.UseAbility(target)
	}

	protected ForceMove(target: UnitX, attack: boolean) {
		const mousePosition = Input.CursorOnWorld

		let movePosition = mousePosition

		const danger = this.OrbwalkMenu.DangerRange.value
		const dangerMouse = this.OrbwalkMenu.DangerMoveToMouse.value

		if (target !== undefined && attack) {

			const targetPosition = target.Position

			if (this.OrbwalkingMode === OrbwalkMode.MOVE_TO_TARGET || this.CanAttack(target, 400)) {
				movePosition = target.IsRanged
					? target.InFrontFromAngle(4, Math.max(200, Math.min(danger, 250)))
					: target.InFront(150)
			}

			if (danger !== 0) {

				const targetDistance = this.Owner.Distance(target)
				const dangerRange = Math.min(this.Owner.AttackRange(target), danger)

				if (dangerMouse) {

					if (targetDistance < dangerRange)
						movePosition = mousePosition

				} else {
					if (targetDistance < dangerRange) {
						const angleBetw = targetPosition.Subtract(this.Owner.Position)
							.AngleBetweenVectors(movePosition.Subtract(targetPosition))
						if (angleBetw < 90) {
							if (angleBetw < 30) {
								movePosition = targetPosition.Extend2D(movePosition, (dangerRange - 25) * -1)
							} else {
								const end = mousePosition
									.Subtract(targetPosition)
									.Rotated(MathSDK.DegreesToRadian(90))
									.MultiplyScalarForThis(dangerRange - 25)
								const right = targetPosition.Add(end)
								const left = targetPosition.Subtract(end)
								movePosition = this.Owner.Distance(right) < this.Owner.Distance(left) ? right : left
							}
						} else if (target.Distance(movePosition) < dangerRange)
							movePosition = targetPosition.Extend2D(movePosition, dangerRange - 25)
					}
				}
			}
		}

		if (this.OrbwalkingMode === 1) {
			if (this.Owner.Distance(movePosition) < 100)
				return false
		} else {
			if (this.Owner.Distance(movePosition) < 10)
				return false
		}

		if (movePosition.Equals(this.LastMovePosition) && this.AttackSleeper.Sleeping)
			return false

		if (!this.FreeMoveSleeper.Sleeping && this.Owner.Move(movePosition)) {
			this.FreeMoveSleeper.Sleep(0.08)
			this.LastMovePosition = movePosition
			return true
		}

		return false
	}

	protected OrbwalkSet(target: UnitX, attack: boolean, move: boolean) {
		if (OrbWalker.Sleeper.Sleeping(this.Owner.Handle))
			return false

		this.LastTarget = target

		if (attack && this.CanAttack(target)) {
			this.LastMovePosition = new Vector3()
			return this.Attack(target)
		}

		if (target !== undefined
			&& !target.IsMoving
			&& this.OrbwalkMenu.OrbwalkerStopOnStanding.value
			&& this.Owner.Distance(target) < this.Owner.GetAttackRange(target))
			return false

		if (move && this.CanMove())
			return this.ForceMove(target, attack)

		return false
	}
}
