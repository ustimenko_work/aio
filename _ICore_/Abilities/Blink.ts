import { Vector3 } from "wrapper/Imports";
import { HitChanceX, OrbWalker } from "immortal-core/Imports";
import { HERO_DATA } from "../data";
import { AIOUsable } from "./Usable";

export interface IUseAIOBlink {
	minDistance: number
	maxDistance: number
	time?: number
}

export class AIOBlink extends AIOUsable {

	public ForceUseAbility() {
		if (!this.Ability.UseAbility(this.Target))
			return false
		const time = this.Ability.GetCastDelay(this.Target) + 0.5
		this.Ability.ActionSleeper.Sleep(time)
		return true
	}

	public CanHit() {
		return true
	}

	public ShouldCast(): boolean {
		if (this.Target === undefined)
			return true
		if (this.Ability.UnitTargetCast && !this.Target.IsVisible)
			return false
		return !(this.Target.HasBuffByName("modifier_pudge_meat_hook"))
	}

	public UseAbility(aoe: boolean, ...args: any[]): boolean {
		if (args === undefined || args.length === 0) {
			new Error("AIOBlink => UseAbility no args...").stack
			return false
		}

		if (args[0] instanceof Vector3)
			return this.UseAbilityPosition(args[0])

		const obj: IUseAIOBlink = {
			minDistance: args[0],
			maxDistance: args[1],
			time: args[2],
		}

		const position = this.Target.PredictedPosition(obj.time ?? 0.2)
			.Extend2D(this.Owner.Position, obj.maxDistance)
		const point = this.Owner.Position
			.Extend2D(position, obj.minDistance)
		if (this.Owner.Distance(point) > this.Owner.Distance(position))
			return false
		if (this.Owner.Distance(position) > this.Ability.CastRange)
			return false
		if (!this.Ability.UseAbility(position, this.TargetManager.EnemyHeroes, HitChanceX.Low))
			return false
		const delay = this.Ability.GetCastDelay(this.Target)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public UseAbilityPosition(toPosition: Vector3) {
		const position = this.Owner.Position.Extend2D(
			toPosition,
			Math.min(this.Ability.CastRange - 25, this.Owner.Distance(toPosition)),
		)
		if (!this.Owner.UseAbility(this.Ability, {intent: position}))
			return false
		const delay = this.Ability.GetCastDelay(position)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
