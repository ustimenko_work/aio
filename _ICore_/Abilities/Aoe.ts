import { HitChanceX, OrbWalker } from "immortal-core/Imports";
import { HERO_DATA } from "../data";
import { AIOUsable } from "./Usable";

export class AIOAoe extends AIOUsable {

	public ShouldCast() {

		if (this.Ability.UnitTargetCast && !this.Target.IsVisible)
			return false

		if (this.Target.IsMagicImmune && !this.Ability.PiercesMagicImmunity(this.Target))
			return false

		if (this.Target.IsInvulnerable) {
			if (this.Ability.UnitTargetCast)
				return false
			if (!this.ChainStun(this.Target, true))
				return false
		}

		if (this.Target.IsRooted && !this.Ability.UnitTargetCast && this.Target.GetImmobilityDuration <= 0)
			return false

		return true
	}

	public ForceUseAbility() {
		if (this.Owner.IsInAbilityPhase)
			return true
		return this.UseAbility(true)
	}

	public UseAbility(aoe: boolean, ...args: any[]) {
		if (this.Owner.IsInAbilityPhase)
			return true
		if (!this.Ability.UseAbility(this.Target, this.TargetManager.EnemyHeroes, HitChanceX.Low))
			return false
		const delay = this.Ability.GetCastDelay(this.Target)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
