import { ActiveAbility, HitChanceX, IDisable, OrbWalker } from "immortal-core/Imports";
import { BitsExtensions, modifierstate } from "wrapper/Imports";
import { HERO_DATA } from "../data";
import { AIOUsable } from "./Usable";

export class AIODisable extends AIOUsable {

	protected AIODisable: ActiveAbility & IDisable

	constructor(ability: ActiveAbility) {
		super(ability)
		this.AIODisable = ability as ActiveAbility & IDisable
	}

	protected get IsHex() {
		return !BitsExtensions.HasBit(this.AIODisable.AppliesUnitState, modifierstate.MODIFIER_STATE_HEXED)
	}

	protected get IsRoot() {
		return !BitsExtensions.HasBit(this.AIODisable.AppliesUnitState, modifierstate.MODIFIER_STATE_ROOTED)
	}

	public ForceUseAbility() {
		if (this.Owner.IsInAbilityPhase)
			return true
		if (!this.Ability.UseAbility(this.Target))
			return false
		const hitTime = this.Ability.GetHitTime(this.Target) + 0.5
		const delay = this.Ability.GetCastDelay(this.Target)
		this.Target.SetExpectedUnitState(this.AIODisable.AppliesUnitState, hitTime)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(hitTime)
		return true
	}

	public ShouldCast(): boolean {
		if (this.AIODisable.UnitTargetCast && !this.Target.IsVisible)
			return false
		if (this.AIODisable.BreaksLinkens && this.Target.IsBlockingAbilities)
			return false
		if (this.Target.IsDarkPactProtected)
			return false
		if (this.Target.IsInvulnerable) {
			if (this.AIODisable.UnitTargetCast)
				return false
			if (!this.ChainStun(this.Target, true))
				return false
		}
		if (this.Ability.GetDamage(this.Target) < this.Target.Health) {
			if (this.Target.IsStunned)
				return this.ChainStun(this.Target, false)
			if (this.Target.IsHexed)
				return this.ChainStun(this.Target, false)
			if (this.Target.IsSilenced)
				return !this.IsSilence(false) || this.ChainStun(this.Target, false)
			if (this.Target.IsRooted)
				return !this.IsRoot || this.ChainStun(this.Target, false)
		}

		if (this.Target.IsRooted && !this.Ability.UnitTargetCast && this.Target.GetImmobilityDuration <= 0)
			return false

		return true
	}

	public UseAbility(aoe: boolean): boolean {
		if (this.Owner.IsInAbilityPhase)
			return true
		if (aoe) {
			if (!this.Ability.UseAbility(this.Target, this.TargetManager.EnemyHeroes, HitChanceX.Low)) {
				return false
			}
		} else {
			if (!this.Ability.UseAbility(this.Target, HitChanceX.Low)) {
				return false
			}
		}
		const hitTime = this.Ability.GetHitTime(this.Target) + 0.5
		const delay = this.Ability.GetCastDelay(this.Target)
		this.Target.SetExpectedUnitState(this.AIODisable.AppliesUnitState, hitTime)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay)
		return true
	}

	public IsSilence(hex: boolean = true) {
		if (!hex && this.IsHex)
			return false
		return !BitsExtensions.HasBit(this.AIODisable.AppliesUnitState, modifierstate.MODIFIER_STATE_SILENCED)
	}
}
