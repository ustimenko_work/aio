import { OrbWalker } from "immortal-core/Imports";
import { HERO_DATA } from "../data";
import { AIOUsable } from "./Usable";

export class AIOTargetable extends AIOUsable {

	public ShouldCast(): boolean {

		if (!this.Target.IsVisible || this.Target.IsInvulnerable || this.Target.IsReflectingDamage
			|| (this.Ability.BreaksLinkens && this.Target.IsBlockingAbilities))
			return false

		return true
	}

	public ForceUseAbility(): boolean {
		return this.UseAbility(true)
	}

	public UseAbility(aoe: boolean, ...args: any[]): boolean {
		if (this.Owner.IsInAbilityPhase)
			return true
		if (!this.Ability.UseAbility(this.Target))
			return false
		const delay = this.Ability.GetCastDelay(this.Target)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
