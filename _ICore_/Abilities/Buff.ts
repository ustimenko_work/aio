import { ActiveAbility, HitChanceX, IBuff, isIToggleable, OrbWalker } from "immortal-core/Imports";
import { HERO_DATA } from "../data";
import { AIOUsable } from "./Usable";

export class AIOBuff extends AIOUsable {

	public Buff: ActiveAbility & IBuff

	constructor(ability: ActiveAbility) {
		super(ability)
		this.Buff = ability as ActiveAbility & IBuff
	}

	public ForceUseAbility() {
		if (this.Owner.IsInAbilityPhase)
			return true
		if (!this.Ability.UseAbility(this.Owner))
			return false
		const delay = this.Ability.GetCastDelay(this.Owner)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public ShouldCast() {
		if (this.Owner.IsInvulnerable)
			return false
		if (this.Owner.Equals(this.Buff.Owner)) {
			if (!this.Buff.BuffsOwner)
				return false
		} else {
			if (!this.Buff.BuffsAlly)
				return false
		}
		if (this.Owner.IsMagicImmune && !this.Buff.PiercesMagicImmunity(this.Owner))
			return false

		if (this.Owner.HasBuffByName(this.Buff.BuffModifierName))
			return false

		if (isIToggleable(this.Buff) && this.Buff.Enabled)
			return false

		return true
	}

	public UseAbility(aoe: boolean, ...args: any[]) {
		if (this.Owner.IsInAbilityPhase)
			return true
		if (typeof args[0] === "number")
			return this.UseAbilityCheckDistance(args[0])
		return this.UseAbility_(aoe)
	}

	private UseAbility_(aoe: boolean, ...args: any[]) {
		if (!this.Ability.UseAbility(this.Owner, this.TargetManager.AllyHeroes, HitChanceX.Low))
			return false
		const delay = this.Ability.GetCastDelay(this.Target)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	private UseAbilityCheckDistance(distance: number) {
		if (this.Owner.Distance(this.Target) > distance)
			return false
		return this.UseAbility_(true)
	}
}
