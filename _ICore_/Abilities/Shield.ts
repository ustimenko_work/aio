import { ActiveAbility, HitChanceX, IShield, isIShield, isIToggleable, OrbWalker } from "immortal-core/Imports";
import { HERO_DATA } from "../data";
import { AIOUsable } from "./Usable";

export class AIOShield extends AIOUsable {

	public Shield: ActiveAbility & IShield

	constructor(ability: ActiveAbility) {
		super(ability)
		this.Shield = ability as ActiveAbility & IShield
	}

	public ForceUseAbility() {
		if (this.Owner.IsInAbilityPhase)
			return true
		if (!this.Ability.UseAbility(this.Owner))
			return false
		if (isIShield(this.Ability) && this.Ability.AppliesUnitState !== undefined)
			this.Owner.SetExpectedUnitState(this.Ability.AppliesUnitState)
		const delay = this.Ability.GetCastDelay(this.Owner)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public ShouldCast() {
		if (this.Owner.IsInvulnerable)
			return false
		if (this.Owner.Equals(this.Shield.Owner)) {
			if (!this.Shield.ShieldsOwner)
				return false
		} else {
			if (!this.Shield.ShieldsAlly)
				return false
		}

		if (this.Owner.IsMagicImmune && !this.Shield.PiercesMagicImmunity(this.Owner))
			return false
		if (this.Owner.HasBuffByName(this.Shield.ShieldModifierName))
			return false
		if (isIToggleable(this.Shield) && this.Shield.Enabled)
			return false

		return true
	}

	public UseAbility(aoe: boolean, ...args: any[]) {
		if (this.Owner.IsInAbilityPhase)
			return true
		if (typeof args[0] === "number")
			return this.UseAbilityCheckDistance(args[0])
		return this.UseAbility_(aoe)
	}

	private UseAbility_(aoe: boolean, ...args: any[]) {

		if (!this.Ability.UseAbility(this.Owner, this.TargetManager.AllyHeroes, HitChanceX.Low))
			return false

		if (isIShield(this.Ability) && this.Ability.AppliesUnitState !== undefined)
			this.Owner.SetExpectedUnitState(this.Ability.AppliesUnitState)

		const delay = this.Ability.GetCastDelay(this.Target)
		HERO_DATA.ComboSleeper.Sleep(delay + 0.1, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	private UseAbilityCheckDistance(distance: number) {
		if (this.Owner.Distance(this.Target) > distance)
			return false
		return this.UseAbility_(true)
	}
}
