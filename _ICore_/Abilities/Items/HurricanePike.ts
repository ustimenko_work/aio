import { HERO_DATA } from "../../data";
import { AIOForceStaff } from "../ForceStaff";

export class AIOHurricanePike extends AIOForceStaff {
	public UseAbilityOnTarget() {
		if (this.Target.IsRanged)
			return false
		if (this.Target.IsLinkensProtected || this.Target.IsInvulnerable || this.Target.IsUntargetable)
			return false
		if (this.Target.IsStunned || this.Target.IsHexed || this.Target.IsRooted || this.Target.IsDisarmed)
			return false
		if (!this.Ability.CanHit(this.Target))
			return false
		if (this.Owner.Distance(this.Target) > 350)
			return false
		if (!this.Ability.UseAbility(this.Target))
			return false
		const delay = this.Ability.CastDelay
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		return true
	}
}
