import { GameState } from "wrapper/Imports";
import { isIToggleable } from "immortal-core/ITypes";
import { AIOShield } from "../Shield";

export class AIOMinotaurHorn extends AIOShield {

	public ShouldCast() {

		const minTime = 0.25
		const Time = GameState.RawGameTime

		const dieTimeBuff = (this.Owner.GetBuffByName("modifier_black_king_bar_immune")?.DieTime ?? 0) - Time
		if (this.Owner.IsInvulnerable || dieTimeBuff > 0.5)
			return false

		if (this.Owner.Equals(this.Shield.Owner)) {
			if (!this.Shield.ShieldsOwner)
				return false
		} else {
			if (!this.Shield.ShieldsAlly)
				return false
		}

		const dieTime = (this.Owner.GetBuffByName(this.Shield.ShieldModifierName)?.DieTime ?? 0) - Time

		if ((this.Owner.IsMagicImmune || dieTime > minTime) && !this.Shield.PiercesMagicImmunity(this.Owner))
			return false

		if (this.Owner.HasBuffByName(this.Shield.ShieldModifierName) && dieTime > minTime)
			return false

		if (isIToggleable(this.Shield) && this.Shield.Enabled)
			return false

		return true
	}
}
