import { UnitX } from "immortal-core/Imports";
import { AIODisable } from "../Disable";

export class AIOBloodthorn extends AIODisable {
	protected ChainStun(target: UnitX, invulnerability: boolean) {
		const remainingTime = target.GetBuffByName("modifier_bloodthorn_debuff")?.RemainingTime ?? 0
		return remainingTime < 0.2
	}
}
