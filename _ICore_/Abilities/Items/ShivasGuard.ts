import { AIODebuff } from "../Debuff";

export class AIOShivasGuard extends AIODebuff {
	public CanHit() {

		if (this.Owner.Distance(this.Target) > 600)
			return false

		if (this.Target.IsMagicImmune && !this.Ability.PiercesMagicImmunity(this.Target))
			return false

		return true
	}
}
