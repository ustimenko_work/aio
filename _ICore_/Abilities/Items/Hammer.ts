import { ActiveAbility, HitChanceX, IChanneled, OrbWalker, UnitX } from "immortal-core/Imports";
import { HERO_DATA } from "../../data";
import { AIODisable } from "../Disable";

export class AIOHammer extends AIODisable {

	public UseAbility(aoe: boolean) {
		const input = this.Ability.GetPredictionInput(this.Target, this.TargetManager.EnemyHeroes)
		input.Delay -= (this.Ability as ActiveAbility & IChanneled).ChannelTime
		const output = this.Ability.GetPredictionOutput(input)
		if (output.HitChance < HitChanceX.Low)
			return false
		if (!this.Ability.UseAbility(this.Target, output.CastPosition))
			return false
		const hitTime = this.Ability.GetHitTime(output.CastPosition) + 0.5
		const delay = this.Ability.GetCastDelay(output.CastPosition)
		this.Target.SetExpectedUnitState(this.AIODisable.AppliesUnitState, hitTime)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(hitTime)
		return true
	}

	public ShouldCast() {
		return !(this.Target.IsStunned || this.Target.IsRooted || this.Target.IsTeleporting)
	}

	protected ChainStun(target: UnitX, invulnerability: boolean) {
		return true
	}
}
