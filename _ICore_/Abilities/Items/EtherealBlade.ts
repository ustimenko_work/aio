import { isIDisable } from "immortal-core/Imports";
import { HERO_DATA } from "../../data";
import { AIODebuff } from "../Debuff";

export class AIOEtherealBlade extends AIODebuff {
	public UseAbility(aoe: boolean) {
		if (!this.Owner.UseAbility(this.Ability, {intent: this.Target}))
			return false
		const hitTime = this.Ability.GetHitTime(this.Target)
		if (isIDisable(this.Ability))
			this.Target.SetExpectedUnitState(this.Ability.AppliesUnitState, hitTime)
		HERO_DATA.ComboSleeper.Sleep(hitTime, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(hitTime)
		return true
	}
}
