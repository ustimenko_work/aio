import { AIODisable } from "../Disable";

export class AIOEulsScepter extends AIODisable {
	private readonly Buffs = [
		"modifier_lina_laguna_blade",
		"modifier_lion_finger_of_death",
	]

	public ShouldCast() {
		if (!this.Target.IsInNormalState && !this.Target.IsTeleporting && !this.Target.IsChanneling)
			return false
		if (this.Target.BaseOwner.ModifiersBook.HasAnyBuffByNames(this.Buffs))
			return false
		return this.ShouldForceCast()
	}

	public ShouldForceCast() {
		if (this.Ability.UnitTargetCast && !this.Target.IsVisible)
			return false
		if (this.AIODisable.UnitTargetCast && this.Target.IsBlockingAbilities)
			return false
		if (this.Target.IsDarkPactProtected)
			return false
		return !(this.Target.IsInvulnerable)
	}
}
