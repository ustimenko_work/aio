import { DemonicPurge, HeroX, isIShield, LotusOrb, MantaStyle, SpiritBearX, UnitX } from "immortal-core/Imports";
import { AIODisable } from "../Disable";

export class AIOOrchid extends AIODisable {
	protected ChainStun(target: UnitX, invulnerability: boolean) {
		const CanEscape = target.Abilities.some(x => (isIShield(x) || x instanceof MantaStyle) && x.Cooldown <= 4)
		const CanLotus = this.TargetManager.AllEnemyUnits.some(x => (x instanceof HeroX || x instanceof SpiritBearX)
			&& (x.GetItemByClass(LotusOrb)?.CanBeCasted(false)
				|| (x.GetAbilityByClass(DemonicPurge)?.BuffsAlly && x.GetAbilityByClass(DemonicPurge)?.CanBeCasted(false))),
		)
		if (CanEscape || CanLotus)
			return true
		return super.ChainStun(target, invulnerability)
	}
}
