import { AIOBuff } from "../Buff";

export class AIOSpeedBuff extends AIOBuff {
	public ShouldCast() {
		if (!super.ShouldCast())
			return false
		if ((!this.Target.IsMoving || this.Target.GetAngle(this.Owner.Position) < 1) && this.Owner.CanAttack(this.Target))
			return false
		return true
	}
}
