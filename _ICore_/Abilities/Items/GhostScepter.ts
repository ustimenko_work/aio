import { AIOShield } from "../Shield";

export class AIOGhostScepter extends AIOShield {
	public ShouldCast() {
		if (this.Owner.GetBuffByName("modifier_minotaur_horn_immune")
			|| this.Owner.GetBuffByName("modifier_black_king_bar_immune"))
			return false
		return true
	}
}
