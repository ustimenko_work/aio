import { HitChanceX, OrbWalker, SkillShotTypeX, UnitX } from "immortal-core/Imports";
import { Vector3 } from "wrapper/Imports";
import { HERO_DATA } from "../../data";
import { AIOBlink } from "../Blink";
import { AIOUsable } from "../Usable";

export class AIOBlinkDagger extends AIOBlink {

	private readonly blinkPosition = new Vector3().Invalidate()

	public ShouldConditionCast(target: UnitX, args: AIOUsable[]) {
		const ability = args[0]
		if (ability === undefined)
			return false

		const input = ability.Ability.GetPredictionInput(target, this.TargetManager.IsValidEnemyHeroes)
		input.Range += this.Ability.CastRange
		input.CastRange = this.Ability.CastRange
		input.SkillShotType = SkillShotTypeX.Circle
		const output = ability.Ability.GetPredictionOutput(input)
		if (output.HitChance < HitChanceX.Low || output.TargetPosition.Distance2D(output.CastPosition) > ability.Ability.Radius * 0.9)
			return false

		this.blinkPosition.CopyFrom(output.CastPosition)

		if (this.Owner.Distance(this.blinkPosition) > this.Ability.CastRange)
			return false

		return true
	}

	public UseAbility(aoe: boolean) {
		if (!this.blinkPosition.IsValid)
			return false
		if (!this.Owner.UseAbility(this.Ability, {intent: this.blinkPosition}))
			return false
		const delay = this.Ability.GetHitTime(this.blinkPosition)
		HERO_DATA.ComboSleeper.Sleep(delay + 0.1, this.Owner.Handle)
		OrbWalker.Sleeper.Sleep(delay + 0.2, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(this.Ability.GetCastDelay(this.Target) + 0.5)
		return true
	}
}
