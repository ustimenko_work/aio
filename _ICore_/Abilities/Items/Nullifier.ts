import { item_aeon_disk } from "wrapper/Imports";
import { AIODebuff } from "../Debuff";

export class AIONullifier extends AIODebuff {
	public ShouldCast() {
		if (this.Ability.UnitTargetCast && !this.Target.IsVisible)
			return false
		if (this.Ability.BreaksLinkens && this.Target.IsBlockingAbilities)
			return false
		if (this.Target.IsDarkPactProtected)
			return false
		if (this.Target.HasBuffByName("modifier_eul_cyclone"))
			return true
		if (this.Target.IsInvulnerable) {
			if (this.AIODebuff.UnitTargetCast)
				return false
			if (!this.ChainStun(this.Target, true))
				return false
		}
		if (this.Target.IsRooted && !this.Ability.UnitTargetCast && this.Target.GetImmobilityDuration <= 0)
			return false

		if (this.Target.Abilities.some(x => x.BaseAbility instanceof item_aeon_disk && x.IsReady))
			return false

		return true
	}
}
