import { AIODebuff } from "../Debuff";

export class AIOMantaStyle extends AIODebuff {
	public ShouldCast() {
		if (!super.ShouldCast())
			return false

		if (this.Target.IsAttackImmune || this.Owner.Distance(this.Target) > this.Owner.GetAttackRange(this.Target, 200))
			return false

		return true
	}
}
