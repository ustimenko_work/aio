import { ActiveAbility, GameSleeperX, HitChanceX, IDebuff, isIDisable, OrbWalker } from "immortal-core/Imports";
import { item_diffusal_blade } from "wrapper/Imports";
import { HERO_DATA } from "../data";
import { AIOUsable } from "./Usable";

export class AIODebuff extends AIOUsable {

	protected AIODebuff: ActiveAbility & IDebuff
	private readonly debuffSleeper = new GameSleeperX<number>();
	private readonly visibleSleeper = new GameSleeperX<number>();

	constructor(ability: ActiveAbility) {
		super(ability)
		this.AIODebuff = ability as ActiveAbility & IDebuff
	}

	public ForceUseAbility() {
		if (this.Owner.IsInAbilityPhase)
			return true
		if (!this.Ability.UseAbility(this.TargetManager.Target!))
			return false
		const delay = this.Ability.GetCastDelay(this.Target)
		if (isIDisable(this.Ability))
			this.Target.SetExpectedUnitState(this.Ability.AppliesUnitState, this.Ability.GetHitTime(this.Target))
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public ShouldCast(): boolean {

		const isVisible = this.Target.IsVisible
		if (this.Ability.UnitTargetCast && !isVisible)
			return false

		if (this.Ability.BaseAbility instanceof item_diffusal_blade && this.Target.GetImmobilityDuration > 0)
			return false

		if (isVisible) {
			if (this.visibleSleeper.Sleeping(this.Target.Handle))
				return false
			const modifier = this.Target.GetBuffByName(this.AIODebuff.DebuffModifierName)
			if (modifier !== undefined) {
				const remainingTime = modifier.RemainingTime
				if (remainingTime === 0)
					return false
				const time = remainingTime - this.Ability.GetHitTime(this.Target)
				if (time > 0) {
					this.debuffSleeper.Sleep(time, this.Target.Handle)
					return false
				}
			}
		} else {
			this.visibleSleeper.Sleep(0.1, this.Target.Handle);
			if (this.debuffSleeper.Sleeping(this.Target.Handle))
				return false
		}

		if (this.Ability.BreaksLinkens && this.Target.IsBlockingAbilities)
			return false

		if (this.Target.IsDarkPactProtected)
			return false

		if (this.Target.IsInvulnerable) {
			if (this.AIODebuff.UnitTargetCast)
				return false
			if (!this.ChainStun(this.Target, true))
				return false
		}

		if (this.Target.IsRooted && !this.Ability.UnitTargetCast && this.Target.GetImmobilityDuration <= 0)
			return false

		return true
	}

	public UseAbility(aoe: boolean): boolean {
		if (this.Owner.IsInAbilityPhase)
			return true
		if (!this.Ability.UseAbility(this.Target, this.TargetManager.EnemyHeroes, HitChanceX.Low))
			return false
		const delay = this.Ability.GetCastDelay(this.Target)
		if (isIDisable(this.Ability)) {
			const hitTime = this.Ability.GetHitTime(this.Target)
			this.Target.SetExpectedUnitState(this.Ability.AppliesUnitState, hitTime)
		}
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
