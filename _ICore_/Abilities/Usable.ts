import { ActiveAbility, TargetManager, UnitX } from "immortal-core/Imports";

export abstract class AIOUsable {

	public Owner!: UnitX
	public Ability!: ActiveAbility
	public TargetManager = TargetManager
	public CycloneBuffs = ["modifier_eul_cyclone", "modifier_wind_waker"]

	constructor(ability: ActiveAbility) {
		this.Ability = ability
		this.Owner = ability.Owner
		this.TargetManager = TargetManager
	}

	public get Target() {
		return this.TargetManager.Target!
	}

	public CanBeCasted(channelingCheck: boolean) {
		return this.Ability.CanBeCasted(channelingCheck)
	}

	public CanHit() {
		return this.Ability.CanHit(this.Target)
	}

	public ShouldConditionCast(target: UnitX, args: Nullable<AIOUsable>[]) {
		return true
	}

	public abstract ShouldCast(): boolean

	public abstract ForceUseAbility(): boolean

	public abstract UseAbility(aoe: boolean, ...args: any[]): boolean

	protected ChainStun(target: UnitX, invulnerability: boolean) {
		const immobile = invulnerability ? target.GetInvulnerabilityDuration : target.GetImmobilityDuration
		if (immobile <= 0)
			return false
		let hitTime = this.Ability.GetHitTime(target)
		if (target.IsHexed || target.IsSilenced || (target.IsStunned && !target.HasAnyBuffByNames(this.CycloneBuffs)) || target.IsRooted)
			hitTime += 0.2
		if (target.IsInvulnerable)
			hitTime -= 0.1
		return hitTime > immobile
	}
}
