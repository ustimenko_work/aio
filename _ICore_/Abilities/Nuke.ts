import { HitChanceX, isIDisable, OrbWalker } from "immortal-core/Imports";
import { HERO_DATA } from "../data";
import { AIOUsable } from "./Usable";

export class AIONuke extends AIOUsable {
	protected breakShields = [
		"modifier_ember_spirit_flame_guard",
		"modifier_item_pipe_barrier",
		"modifier_abaddon_aphotic_shield",
		"modifier_oracle_false_promise_timer",
	]

	public ForceUseAbility(): boolean {
		if (this.Owner.IsInAbilityPhase)
			return true

		if (!this.Ability.UseAbility(this.Target))
			return false

		const delay = this.Ability.GetCastDelay(this.Target)
		if (isIDisable(this.Ability))
			this.Target.SetExpectedUnitState(this.Ability.AppliesUnitState, this.Ability.GetHitTime(this.Target))

		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public ShouldCast(): boolean {
		if (this.Ability.UnitTargetCast && !this.Target.IsVisible)
			return false
		if (this.Target.IsReflectingDamage)
			return false
		if (this.Ability.BreaksLinkens && this.Target.IsBlockingAbilities)
			return false
		const damage = this.Ability.GetDamage(this.Target)
		if (damage <= 0 && !this.Target.BaseOwner.ModifiersBook.HasAnyBuffByNames(this.breakShields))
			return false
		if (this.Target.IsInvulnerable) {
			if (this.Ability.UnitTargetCast)
				return false
			if (!this.ChainStun(this.Target, true))
				return false
		}

		if (this.Target.IsRooted && !this.Ability.UnitTargetCast && this.Target.GetImmobilityDuration <= 0)
			return false

		return true
	}

	public UseAbility(aoe: boolean): boolean {
		if (this.Owner.IsInAbilityPhase)
			return true
		if (aoe) {
			if (!this.Ability.UseAbility(this.Target, this.TargetManager.EnemyHeroes, HitChanceX.Low))
				return false
		} else {
			if (!this.Ability.UseAbility(this.Target, HitChanceX.Low))
				return false
		}
		const delay = this.Ability.GetCastDelay(this.Target)
		if (isIDisable(this.Ability))
			this.Target.SetExpectedUnitState(this.Ability.AppliesUnitState, this.Ability.GetHitTime(this.Target))
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
