import { AbilityX, GameSleeperX, OnExecuteOrderX, ParticlesX, UnitX } from "immortal-core/Imports"
import { ExecuteOrder, Unit, VKeys, VMouseKeys } from "wrapper/Imports"

export interface HeroModule {
	onGameEnded(): void

	onDraw(unit: UnitX): void

	onKeyDown(key: VKeys): boolean

	onMouseDown(key: VMouseKeys): boolean

	onOrders(order: ExecuteOrder): boolean

	onOrdersScript(order: OnExecuteOrderX): void

	onTick(unit: UnitX): void

	onCreated(unit: UnitX): void

	onDestoyed(unit: UnitX): void

	onAbilityCreated(unit: UnitX, abil: AbilityX): void

	onAbilityDestroyed(unit: UnitX, abil: AbilityX): void
}

export enum ModeAttack {
	Never,
	Standart,
	Always
}

export class HERO_DATA {

	public static ComboSleeper = new GameSleeperX()
	public static ParticleManager = new ParticlesX()
	public static HeroModules = new Map<string | Constructor<Unit>, HeroModule>()
	private static NameAutoAttackMode = "dota_player_units_auto_attack_mode"

	public static get ModeAttack(): ModeAttack {
		return ConVars.GetInt(this.NameAutoAttackMode)
	}

	public static set ModeAttack(value) {
		ConVars.Set(this.NameAutoAttackMode, value)
	}

	public static GetHeroModule(unit: UnitX | Unit): Nullable<HeroModule> {
		const name = unit.Name
		if (this.HeroModules.has(name))
			return this.HeroModules.get(name)
		if (unit instanceof UnitX)
			unit = unit.BaseOwner
		return this.HeroModules.get(unit.constructor as Constructor<Unit>)
	}

	public static Dispose() {
		this.DestroyAllParticle()
		this.ComboSleeper.FullReset()
	}

	private static DestroyAllParticle() {
		this.ParticleManager.AllParticles.forEach(particle => {
			if (particle === undefined || !particle.IsValid)
				return
			particle.Destroy(true)
		})
	}
}
