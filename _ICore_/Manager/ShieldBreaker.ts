import {
	ActiveAbility,
	EventsX,
	GameSleeperX,
	isIDisable,
	OrbWalker,
	TargetManager,
	UnitX,
} from "immortal-core/Imports";
import { ArrayExtensions } from "wrapper/Imports";
import { ModeCombo, ShieldsMenu } from "../../Menu/Base";

const linkensSleeper = new GameSleeperX()
export const ShieldBreakerMap = new Map<UnitX, ShieldBreaker>()

export class ShieldBreaker {

	public CanHitSphere = false

	constructor(private owner: UnitX, private menu: ShieldsMenu) {
	}

	public Update(menuMode: ModeCombo) {

		const target = TargetManager.Target!

		if (target === undefined || TargetManager.TargetSleeper.Sleeping)
			return

		let abilities = this.owner.Abilities.filter(abil => abil instanceof ActiveAbility
			&& (this.menu.Shields.IsEnabled(abil.Name) || this.menu.Sphere.IsEnabled(abil.Name))
			&& abil.UnitTargetCast && abil.BreaksLinkens && abil.CanBeCasted() && !abil.IsFake) as ActiveAbility[]

		abilities = ArrayExtensions.orderByRevert(abilities.filter(abil => abil.IsValid), x => x.ID)
		abilities = ArrayExtensions.orderBy(abilities.filter(abil => abil.IsValid), x => x.CastPoint)

		this.CanHitSphere = abilities.some(x => this.ShouldUseBreaker(x, target, menuMode))

		abilities.forEach(ability => {

			if (!ability.CanHit(target) || !this.ShouldUseBreaker(ability, target, menuMode))
				return
			if (ability.UseAbility(target)) {
				linkensSleeper.Sleep(ability.GetHitTime(target) + 5.5, target.Handle)
				OrbWalker.Sleeper.Sleep(ability.GetCastDelay(target), this.owner.Handle)
				TargetManager.TargetSleeper.Sleep(ability.GetHitTime(target) + 0.1)
			}
		})
	}

	protected ShouldUseBreaker(ability: ActiveAbility, target: UnitX, menu: ModeCombo) {
		if (!ability.UnitTargetCast || linkensSleeper.Sleeping(target.Handle))
			return false

		if (target.IsUntargetable || !target.IsVisible || target.IsInvulnerable)
			return false

		if (this.owner.Abilities.some(x => isIDisable(x)
			&& (menu.Abilities.IsEnabled(x.Name) || menu.Items.IsEnabled(x.Name))
			&& x.NoTargetCast && x.CanBeCasted() && x.CanHit(target)))
			return false

		if ((target.IsLinkensProtected && target.IsLotusProtected) || target.IsSpellShieldProtected)
			return this.menu.Shields.IsEnabled(ability.Name)

		if (target.IsLinkensProtected && this.menu.Sphere.IsEnabled(ability.Name))
			return true

		return false
	}
}

EventsX.on("GameEnded", () => {
	linkensSleeper.FullReset()
	ShieldBreakerMap.clear()
})
