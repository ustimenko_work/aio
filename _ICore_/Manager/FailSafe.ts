import { KillSteal } from "immortal-kill-steal/Imports"
import {
	AbilitiesX,
	AbilityX,
	ActiveAbility,
	AreaOfEffectAbility,
	CircleAbility,
	ConeAbility,
	EventsX,
	GameX,
	HitChanceX,
	isIBlink,
	isIHasRadius,
	LineAbility,
	OrbWalker,
	PCircle,
	Polygon,
	PRectangle,
	PredictionAbility,
	PTrapezoid,
	TargetManager,
	TickSleeperX,
	UnitXManager,
} from "immortal-core/Imports"
import { dotaunitorder_t, EventsSDK, GameState, Vector3 } from "wrapper/Imports"
import { HERO_DATA } from "../data"

export class FailSafeManager {

	public static IsEnabled = false
	public static Sleeper = new TickSleeperX()
	public static abilityPositions = new Map<number, Vector3>()
	public static abilityTimings = new Map<ActiveAbility, number>()

	private static ignoredAbilities: string[] = [
		"arc_warden_magnetic_field",
		"storm_spirit_ball_lightning",
		"leshrac_diabolic_edict",
		"shredder_timber_chain",
		"magnataur_skewer",
		"disruptor_kinetic_field",
		"nevermore_requiem",
		"disruptor_static_storm",
		"crystal_maiden_freezing_field",
		"skywrath_mage_mystic_flare",
		"phoenix_icarus_dive",
		"kunkka_torrent",
		"kunkka_ghostship",
		"elder_titan_echo_stomp_spirit",
		"elder_titan_echo_stomp",
		"bloodseeker_blood_bath",
		"phantom_lancer_doppelwalk",
		"mars_gods_rebuke",
		"beastmaster_wild_axes",
	]

	public static IsIgnored(abil: AbilityX) {
		if (this.ignoredAbilities.includes(abil.Name))
			return true
		return isIBlink(abil)
	}

	public static OnTick() {
		if (this.Sleeper.Sleeping || !this.IsEnabled)
			return

		const target = TargetManager.TargetLocked ? TargetManager.Target : KillSteal?.Target

		if (target?.IsValid !== true || !target.IsVisible)
			return

		this.abilityTimings.forEach((time, ability) => {

			if (!ability.IsValid || !ability.IsInAbilityPhase)
				return

			const owner = ability.Owner
			const input = ability.GetPredictionInput(target)
			input.Delay = Math.max(time - GameState.RawGameTime, 0) + ability.ActivationDelay
			const output = ability.GetPredictionOutput(input)

			if (!isIHasRadius(ability))
				return

			const castPosition = this.abilityPositions.get(ability.Handle)

			if (castPosition === undefined)
				return

			let polygon: Nullable<Polygon>
			if (ability instanceof CircleAbility || ability instanceof AreaOfEffectAbility)
				polygon = new PCircle(castPosition, ability.Radius + 50)

			if (ability instanceof ConeAbility)
				polygon = new PTrapezoid(owner.Position.Extend2D(castPosition, -ability.Radius / 2),
					owner.Position.Extend2D(castPosition, ability.Range),
					ability.Radius + 50,
					ability.EndRadius + 100)

			if (ability instanceof LineAbility)
				polygon = new PRectangle(owner.Position, owner.Position.Extend2D(castPosition, ability.Range), ability.Radius + 50)

			if (polygon === undefined)
				return

			if (!target.IsAlive || output.HitChance === HitChanceX.Impossible || !polygon.IsInside(output.TargetPosition)) {
				this.Sleeper.Sleep(0.15)
				this.abilityTimings.delete(ability)
				this.abilityPositions.delete(ability.Handle)
				OrbWalker.Sleeper.ResetKey(ability.Owner.Handle)
				ability.ActionSleeper.ResetTimer()
				KillSteal.KillStealSleeper.ResetTimer()
				target.RefreshUnitState()
				ability.Owner.Stop()
				HERO_DATA.ComboSleeper.ResetKey(ability.Owner.Handle)
			}
		})
	}

	public static Dispose() {
		this.IsEnabled = false
		this.Sleeper.ResetTimer()
		this.abilityTimings.clear()
		this.abilityPositions.clear()
	}
}

EventsSDK.on("Tick", () => {
	if (!GameX.IsInGame)
		return
	UnitXManager.HeroesX.forEach(hero => {
		if (!hero.IsControllable || hero.IsEnemy())
			return
		const abilities = hero.Abilities.filter(x => x instanceof ActiveAbility) as ActiveAbility[]
		abilities.forEach(ability => {
			if (!ability.IsInAbilityPhase) {
				FailSafeManager.abilityTimings.delete(ability)
				FailSafeManager.abilityPositions.delete(ability.Handle)
				if (FailSafeManager.abilityTimings.size <= 0)
					FailSafeManager.IsEnabled = false
				return
			}
			if (FailSafeManager.IsEnabled)
				return
			if (FailSafeManager.IsIgnored(ability))
				return
			if (!(ability instanceof AreaOfEffectAbility) && !(ability instanceof PredictionAbility))
				return
			if (ability instanceof AreaOfEffectAbility) {
				if (ability.CastRange > 0) {
					FailSafeManager.abilityPositions.set(ability.Handle, ability.Owner.InFront(ability.CastRange))
				} else
					FailSafeManager.abilityPositions.set(ability.Handle, ability.Owner.Position)
			}
			FailSafeManager.abilityTimings.set(ability, GameState.RawGameTime + ability.CastPoint)
			FailSafeManager.IsEnabled = true
		})
	})
})

EventsX.on("OnExecuteOrder", order => {
	if (order.orderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION || order.ability === undefined)
		return
	const ability = AbilitiesX.All.find(x => order.ability!.Handle === x.Handle)
	if (ability === undefined || order.position === undefined)
		return
	FailSafeManager.abilityPositions.set(ability.Handle, order.position)
})

EventsSDK.on("Tick", () => FailSafeManager.OnTick())
EventsX.on("GameEnded", () => FailSafeManager.Dispose())
