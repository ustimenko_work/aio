import { TargetManager, UnitX } from "immortal-core/Imports";
import { Input, Vector3 } from "wrapper/Imports";
import { ModeCombo } from "../../Menu/Base";
import { AIOBlink } from "../Abilities/Blink";
import { AIOBuff } from "../Abilities/Buff";
import { AIOForceStaff } from "../Abilities/ForceStaff";
import { AIOShield } from "../Abilities/Shield";
import { AIOUsable } from "../Abilities/Usable";
import { HERO_DATA } from "../data";

export class AbilityHelper {

	constructor(public unit: UnitX, public menu?: ModeCombo, private ignoreEnabledMenu: boolean = false, private ownerCheck: boolean = false) {
	}

	public CanBeCasted(
		ability: Nullable<AIOUsable>,
		canHit: boolean = true,
		shouldCast: boolean = true,
		channelingCheck: boolean = true,
		canBeCastedCheck: boolean = true,
	) {

		if ((this.unit.BaseOwner.IsInvisible && !this.unit.BaseOwner.IsVisibleForEnemies) && !(this.menu?.IgnoreInvis.value ?? true))
			return false

		if (ability === undefined)
			return false

		if (!this.IsEnableMenu(ability))
			return false

		if (this.ownerCheck && this.unit.Handle !== ability.Owner.Handle)
			return false

		if (canBeCastedCheck && !ability.CanBeCasted(channelingCheck))
			return false

		if (canHit && !ability.CanHit())
			return false

		if (shouldCast && !ability.ShouldCast())
			return false

		return true
	}

	public HasMana(...abilities: Nullable<AIOUsable>[]) {
		return this.MissingMana(abilities) <= 0
	}

	public IsEnableMenu(ability: Nullable<AIOUsable>) {
		if (ability === undefined)
			return false
		return !(!this.ignoreEnabledMenu &&
			(
				(ability.Ability.IsItem && (!this.menu?.Items.IsEnabled(this.SmartSelectedItem(ability) ?? true)))
				||
				(!ability.Ability.IsItem && (!this.menu?.Abilities.IsEnabled(this.SmartSelectedAbility(ability)) ?? true))
			)
		)
	}

	public UseAbility(ability: Nullable<AIOUsable>, ...args: (boolean | number | Vector3)[]) {

		if (ability === undefined || !this.CanBeCasted(ability))
			return false

		if (args.length === 0)
			return ability.UseAbility(true)

		if (typeof args[0] === "boolean")
			return ability.UseAbility(args[0])

		if (ability instanceof AIOBlink)
			return this.UseBlinkAbility(ability, ...args)

		if (ability instanceof AIOBuff)
			return this.UseIBuffAbility(ability, ...args)

		if (ability instanceof AIOShield)
			return this.UseAIOShieldAbility(ability, ...args)
	}

	public UseAbilityIfCondition(ability: Nullable<AIOUsable>, ...args: Nullable<AIOUsable>[]) {
		if (!this.CanBeCasted(ability))
			return false

		const abilities = args.filter(x => this.CanBeCasted(x, false, false))
		if (!ability!.ShouldConditionCast(TargetManager.Target!, abilities))
			return false

		return ability!.UseAbility(true)
	}

	public UseAbilityIfNone(ability: Nullable<AIOUsable>, ...args: Nullable<AIOUsable>[]) {

		if (!this.CanBeCasted(ability))
			return false

		if (args.every(x => !this.CanBeCasted(x, false)))
			return ability!.UseAbility(true)

		return false
	}

	public ForceUseAbility(ability: Nullable<AIOUsable>, ignorePrediction = false, aoe = true) {
		return ignorePrediction
			? ability?.ForceUseAbility()
			: ability?.UseAbility(aoe)
	}

	public UseKillStealAbility(ability: Nullable<AIOUsable>, aoe = true) {
		if (!this.CanBeCasted(ability))
			return false
		const target = TargetManager.Target
		if (target === undefined)
			return false
		if (ability!.Ability.GetDamage(target) < target.Health)
			return false
		return ability!.UseAbility(aoe)
	}

	public MissingMana(abilities: Nullable<AIOUsable>[]) {
		const manaCost = abilities.filter(x => x?.Ability.IsValid === true)
			.map(x => x!.Ability.ManaCost)
			.reduce((prev, curr) => curr + prev, 0)
		return manaCost - this.unit.Mana
	}

	public CanBeCastedIfCondition(ability: Nullable<AIOUsable>, ...args: Nullable<AIOUsable>[]) {
		if (!this.CanBeCasted(ability))
			return false

		if (!ability!.ShouldConditionCast(TargetManager.Target!, args.filter(x => this.CanBeCasted(x, false, false))))
			return false

		return true
	}

	public UseDoubleBlinkCombo(force: Nullable<AIOForceStaff>, blink: Nullable<AIOBlink>, minDistance: number = 0) {
		if (!this.CanBeCasted(force, false) || !this.CanBeCasted(blink, false))
			return false

		if (force === undefined || blink === undefined)
			return false

		const target = TargetManager.Target!
		const owner = force.Ability.Owner

		if (owner.Distance(target) < minDistance || owner.Distance(target) < blink.Ability.Range)
			return false

		const range = blink.Ability.Range + force.Ability.Range

		if (owner.Distance(target) > range)
			return false

		if (owner.GetAngle(target.Position) > 0.5) {
			owner.Move(target.Position)
			HERO_DATA.ComboSleeper.Sleep(0.1, this.unit.Handle)
			return false
		}

		force.Ability.UseAbility(owner)
		HERO_DATA.ComboSleeper.Sleep(force.Ability.CastDelay + 0.3, this.unit.Handle)
		return true
	}

	public UseAbilityIfAny(ability: Nullable<AIOUsable>, ...args: Nullable<AIOUsable>[]) {
		if (!this.CanBeCasted(ability))
			return false

		if (args.some(x => this.CanBeCasted(x, false)))
			return ability!.UseAbility(true)

		return false
	}

	public UseAbilityIfConditionAnyNotNone(ability: Nullable<AIOUsable>, ...args: Nullable<AIOUsable>[]) {
		if (ability === undefined || !this.CanBeCasted(ability))
			return false
		const NotFound = args.filter(x => x === undefined)
		const CanBeCasted = args.filter(x => this.CanBeCasted(x, false, false))
		const NotCanBeCasted = args.filter(x => !this.CanBeCasted(x, false, false))
		if (CanBeCasted.length > 0 || NotCanBeCasted.length === NotFound.length)
			return false
		return ability.UseAbility(true)
	}

	public UseForceStaffAway(force: Nullable<AIOForceStaff>, range: number) {
		if (!this.CanBeCasted(force))
			return false
		const target = TargetManager.Target!
		if (target.IsRanged || target.IsStunned || target.IsRooted || target.IsHexed || target.IsDisarmed)
			return false
		const owner = force!.Ability.Owner
		if (target.Distance(owner) > range)
			return false
		const mouse = Input.CursorOnWorld
		if (owner.GetAngle(mouse) > 1) {
			owner.Move(mouse)
			force!.Ability.UseAbility(owner)
			return true
		}
		return false
	}

	private UseBlinkAbility(ability: AIOBlink, ...args: (boolean | number | Vector3)[]) {
		if (!this.CanBeCasted(ability))
			return false
		return ability.UseAbility(false, ...args)
	}

	private UseIBuffAbility(ability: AIOBuff, ...args: (boolean | number | Vector3)[]) {
		if (!this.CanBeCasted(ability))
			return false
		return ability.UseAbility(false, ...args)
	}

	private UseAIOShieldAbility(ability: AIOShield, ...args: (boolean | number | Vector3)[]) {
		if (!this.CanBeCasted(ability))
			return false
		return ability.UseAbility(false, ...args)
	}

	private SmartSelectedAbility(abil: AIOUsable) {
		let abils = ""
		// fix custom game
		if (!abil.Ability.IsItem && abil.Ability.Name.includes("pudge_meat_hook")) {
			abils = "pudge_meat_hook"
		} else if (!abil.Ability.IsItem)
			abils = abil.Ability.Name
		return abils
	}

	private SmartSelectedItem(abil: AIOUsable) {
		let item = ""
		if (abil.Ability.IsItem && abil.Ability.Name.includes("dagon")) {
			item = "item_dagon_5"
		} else if (abil.Ability.IsItem && abil.Ability.Name.includes("item_rod_of_atos")) {
			item = "item_rod_of_atos"
		} else if (abil.Ability.IsItem && abil.Ability.Name.includes("blink")) {
			item = "item_blink"
		} else if (abil.Ability.IsItem && (abil.Ability.Name.includes("cyclone") || abil.Ability.Name.includes("wind_waker"))) {
			item = "item_cyclone"
		} else if (abil.Ability.IsItem)
			item = abil.Ability.Name
		return item
	}
}
