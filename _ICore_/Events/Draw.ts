import { GameX, TargetManager } from "immortal-core/Imports"
import { DOTAGameUIState_t, EventsSDK, GameState } from "wrapper/Imports"
import { HERO_DATA } from "../data"

EventsSDK.on("Draw", () => {
	if (!GameX.IsInGame || GameState.UIState !== DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME)
		return
	TargetManager.AllyHeroes.forEach(unit => {
		if (unit.IsControllable && !unit.IsEnemy())
			HERO_DATA.GetHeroModule(unit)?.onDraw(unit)
	})
})
