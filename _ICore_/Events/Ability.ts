import { EventsX, HeroX } from "immortal-core/Imports";
import { HERO_DATA } from "../data";

EventsX.on("AbilityCreated", abil => {
	if (!(abil.Owner instanceof HeroX) || abil.Owner.IsEnemy() || !abil.IsControllable)
		return
	HERO_DATA.GetHeroModule(abil.Owner)?.onAbilityCreated(abil.Owner, abil)
})

EventsX.on("AbilityDestroyed", abil => {
	if (!(abil.Owner instanceof HeroX) || abil.Owner.IsEnemy() || !abil.IsControllable)
		return
	HERO_DATA.GetHeroModule(abil.Owner)?.onAbilityDestroyed(abil.Owner, abil)
})

EventsX.on("ItemChanged", (item, hero, remove) => {
	if (!(hero instanceof HeroX) || hero.IsEnemy() || !hero.IsControllable)
		return
	!remove ? HERO_DATA.GetHeroModule(hero)?.onAbilityCreated(hero, item)
		: HERO_DATA.GetHeroModule(hero)?.onAbilityDestroyed(hero, item)
})
