import { EventsX, GameX, UnitX } from "immortal-core/Imports"
import { Ability, dotaunitorder_t, EventsSDK, Unit } from "wrapper/Imports"
import { HERO_DATA } from "../data"

EventsSDK.on("PrepareUnitOrders", order => {
	if (!GameX.IsInGame
		|| !(order.Issuers[0] instanceof Unit)
		|| order.Issuers[0].IsEnemy()
		|| order.OrderType === dotaunitorder_t.DOTA_UNIT_ORDER_TRAIN_ABILITY)
		return true

	if (order.OrderType === dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
		&& order.Ability instanceof Ability
		&& order.Ability.IsItem
		&& order.Ability.MaxChannelTime === 0)
		return true

	const hero_module = HERO_DATA.GetHeroModule(order.Issuers[0])
	if (hero_module === undefined)
		return true
	return hero_module.onOrders(order)
})

EventsX.on("OnExecuteOrder", order => {
	if (!GameX.IsInGame || !(order.issuers instanceof UnitX)
		|| order.issuers.IsEnemy()
		|| order.orderType === dotaunitorder_t.DOTA_UNIT_ORDER_TRAIN_ABILITY)
		return
	const hero_module = HERO_DATA.GetHeroModule(order.issuers)
	if (hero_module === undefined)
		return
	hero_module.onOrdersScript(order)
})
