import { GameX, TargetManager } from "immortal-core/Imports"
import { EventsSDK } from "wrapper/Imports"
import { HERO_DATA } from "../data"

EventsSDK.on("Tick", () => {
	if (!GameX.IsInGame)
		return
	TargetManager.Heroes.forEach(unit => {
		if (unit.IsControllable && !unit.IsEnemy())
			HERO_DATA.GetHeroModule(unit)?.onTick(unit)
	})
})
