import { EventsX, TargetManager } from "immortal-core/Imports";
import { HERO_DATA } from "../data";

EventsX.on("GameEnded", () => {
	HERO_DATA.Dispose()
	TargetManager.Dispose()
	TargetManager.Heroes.forEach(unit => HERO_DATA.GetHeroModule(unit)?.onGameEnded())
})
